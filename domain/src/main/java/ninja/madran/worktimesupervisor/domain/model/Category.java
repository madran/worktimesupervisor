package ninja.madran.worktimesupervisor.domain.model;

public record Category(Long id, String name, Project project, Boolean isDeleted) {

    public Category(String name, Project project) {

        this(0L, name, project, false);
    }
}
