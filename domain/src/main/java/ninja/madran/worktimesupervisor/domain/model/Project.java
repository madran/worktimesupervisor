package ninja.madran.worktimesupervisor.domain.model;

public record Project(long id, String name, String color) {

    public Project(String name, String color) {

        this(0L, name, color);
    }

    public Project(String name) {

        this(0L, name, "FFFFFF");
    }
}
