package ninja.madran.worktimesupervisor.domain.model;

import java.sql.Timestamp;

public record Phase(Long id, Timestamp startTime, Timestamp endTime, Task task, Timestamp savedAt) {

    public Phase(Timestamp startTime, Timestamp endTime, Task task) {

        this(0L, startTime, endTime, task, null);
    }
}
