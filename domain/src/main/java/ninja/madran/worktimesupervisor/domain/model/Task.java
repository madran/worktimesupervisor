package ninja.madran.worktimesupervisor.domain.model;

import java.sql.Timestamp;

public record Task(Long id, String name, Timestamp timeOfLastUse, Category category, Boolean isDeleted) {

    public Task(String name, Timestamp timeOfLastUse, Category category) {

        this(0L, name, timeOfLastUse, category, false);
    }
}
