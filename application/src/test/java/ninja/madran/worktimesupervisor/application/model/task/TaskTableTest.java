package ninja.madran.worktimesupervisor.application.model.task;

import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.lib.test.IntegrationTest;
import ninja.madran.worktimesupervisor.application.model.category.CategoryTable;
import ninja.madran.worktimesupervisor.domain.model.Category;
import ninja.madran.worktimesupervisor.domain.model.Task;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TaskTableTest {

    CategoryTable categoryTable = (CategoryTable) ServiceContainer.INSTANCE.get(CategoryTable.class.getName());

    TaskTable taskTable = (TaskTable) ServiceContainer.INSTANCE.get(TaskTable.class.getName());

    @BeforeAll
    static void beforeAll() throws SQLException {

        IntegrationTest.buildDb();
    }

    @AfterEach
    void afterEach() throws SQLException {

        IntegrationTest.buildDb();
    }

    @Test
    public void create() throws SQLException {

        Category categoryA = categoryTable.find(1).orElseThrow();
        Category categoryB = categoryTable.find(2).orElseThrow();


        Task taskA = new Task(
                "TaskA",
                Timestamp.from(Instant.now()),
                categoryA
        );

        Task taskB = new Task(
                "TaskB",
                Timestamp.from(Instant.now()),
                categoryB
        );

        taskA = taskTable.create(taskA).orElseThrow();
        taskB = taskTable.create(taskB).orElseThrow();

        assertEquals(8, taskA.id());
        assertEquals("CategoryA", taskA.category().name());
        assertEquals("ProjectA", taskA.category().project().name());
        assertEquals(9, taskB.id());
        assertEquals("CategoryB", taskB.category().name());
        assertEquals("ProjectA", taskB.category().project().name());
    }

    @Test
    void find() throws SQLException {

        Task task = taskTable.find(1).orElseThrow();

        assertEquals(1, task.id());
        assertEquals("CategoryA", task.category().name());
        assertEquals("ProjectA", task.category().project().name());
    }

    @Test
    void update() throws SQLException {

        Task task = taskTable.find(1).orElseThrow();

        Task newTask = new Task(
                task.id(),
                "Update",
                task.timeOfLastUse(),
                task.category(),
                task.isDeleted()
        );

        int result = taskTable.update(newTask);

        assertEquals(1, result);
    }

    @Test
    void findAll() throws SQLException {

        List<Task> tasks = taskTable.findAll();

        assertEquals(tasks.size(), 7);
        assertEquals(1, tasks.get(0).id());
        assertEquals("CategoryA", tasks.get(0).category().name());
        assertEquals("ProjectA", tasks.get(0).category().project().name());
        assertEquals(2, tasks.get(1).id());
        assertEquals("CategoryB", tasks.get(1).category().name());
        assertEquals("ProjectB", tasks.get(1).category().project().name());
        assertEquals(3, tasks.get(2).id());
        assertEquals("CategoryB", tasks.get(1).category().name());
        assertEquals("ProjectB", tasks.get(1).category().project().name());
    }

    @Test
    void delete() throws SQLException {

        int result = taskTable.delete(1);

        assertEquals(1, result);
    }

    @Test
    void findRecentTasksByCategoryId() throws SQLException {

        Category category = categoryTable.find(1).orElseThrow();

        List<Task> tasks = taskTable.findAllRecentTasksByCategoryId(category.id(), 1);

        assertEquals(1, tasks.size());
    }

    @Test
    void addTaskWithNotUniqueName() throws SQLException {

        Category categoryA = categoryTable.find(1).orElseThrow();
        Category categoryB = categoryTable.find(2).orElseThrow();


        Task taskA = new Task(
                "unique",
                Timestamp.from(Instant.now()),
                categoryA
        );

        Task taskB = new Task(
                "unique",
                Timestamp.from(Instant.now()),
                categoryB
        );

        Task taskC = new Task(
                "unique",
                Timestamp.from(Instant.now()),
                categoryB
        );

        taskTable.create(taskA).orElseThrow();
        taskTable.create(taskB).orElseThrow();

        assertThrows(SQLException.class, () -> taskTable.create(taskC).orElseThrow());

    }
}