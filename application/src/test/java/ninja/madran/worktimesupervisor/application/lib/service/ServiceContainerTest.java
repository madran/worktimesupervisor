package ninja.madran.worktimesupervisor.application.lib.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ServiceContainerTest {

    @Test
    public void add() {

        ServiceContainer serviceContainer = ServiceContainer.INSTANCE;

        serviceContainer.add("key", "value");

        assertEquals("value", serviceContainer.get("key"));
    }
}