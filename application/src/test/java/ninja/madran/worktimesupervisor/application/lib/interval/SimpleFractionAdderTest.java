package ninja.madran.worktimesupervisor.application.lib.interval;

import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SimpleFractionAdderTest {

    @Test
    public void sumEmptyFractionCollection() {

        TimeFractionCollection<Long> timeFractionCollection = new TimeFractionCollection<>(1L);

        SimpleFractionAdder<Long> adder = new SimpleFractionAdder<>();

        TimeInterval<Long> timeInterval = adder.sum(timeFractionCollection);

        assertEquals(1L, timeInterval.getId());
        assertEquals(0L, timeInterval.getDuration().getSeconds());
    }

    @Test
    public void sumOneFraction() {

        LocalDateTime startDate = LocalDateTime.of(2000, 1, 1, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2000, 1, 1, 1, 0);

        TimeFraction timeFraction = new TimeFraction(Timestamp.valueOf(startDate), Timestamp.valueOf(endDate));

        TimeFractionCollection<Long> timeFractionCollection = new TimeFractionCollection<>(1L);
        timeFractionCollection.add(timeFraction);

        SimpleFractionAdder<Long> adder = new SimpleFractionAdder<>();

        TimeInterval<Long> timeInterval = adder.sum(timeFractionCollection);

        assertEquals(1L, timeInterval.getId());
        assertEquals(3600L, timeInterval.getDuration().getSeconds());
    }

    @Test
    public void sumThreeFractions() {

        LocalDateTime startDate = LocalDateTime.of(2000, 1, 1, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2000, 1, 1, 1, 0);

        TimeFraction timeFractionA = new TimeFraction(Timestamp.valueOf(startDate), Timestamp.valueOf(endDate));
        TimeFraction timeFractionB = new TimeFraction(Timestamp.valueOf(startDate), Timestamp.valueOf(endDate));
        TimeFraction timeFractionC = new TimeFraction(Timestamp.valueOf(startDate), Timestamp.valueOf(endDate));

        TimeFractionCollection<Long> timeFractionCollection = new TimeFractionCollection<>(1L);
        timeFractionCollection.add(timeFractionA);
        timeFractionCollection.add(timeFractionB);
        timeFractionCollection.add(timeFractionC);

        SimpleFractionAdder<Long> adder = new SimpleFractionAdder<>();

        TimeInterval<Long> timeInterval = adder.sum(timeFractionCollection);

        assertEquals(1L, timeInterval.getId());
        assertEquals(3600L * 3, timeInterval.getDuration().getSeconds());
    }
}