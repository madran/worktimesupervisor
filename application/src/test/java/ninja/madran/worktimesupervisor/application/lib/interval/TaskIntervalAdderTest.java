package ninja.madran.worktimesupervisor.application.lib.interval;

import ninja.madran.worktimesupervisor.domain.model.Phase;
import ninja.madran.worktimesupervisor.domain.model.Task;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TaskIntervalAdderTest {

    @Test
    public void getTimeIntervals() {

        Task task = new Task(
                1L,
                "task",
                Timestamp.from(Instant.now()),
                null,
                false

        );

        LocalDateTime startDate = LocalDateTime.of(2000, 1, 1, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2000, 1, 1, 1, 0);

        Phase phase = new Phase(
                1L,
                Timestamp.valueOf(startDate),
                Timestamp.valueOf(endDate),
                task,
                Timestamp.from(Instant.now())
        );

        List<Phase> phases = new ArrayList<>();
        phases.add(phase);

        TaskIntervalAdder taskIntervalAdder = new TaskIntervalAdder(phases);

        Map<Long, Duration> timeIntervals = taskIntervalAdder.getTimeIntervals();

        assertEquals(3600, timeIntervals.get(1L).getSeconds());
    }
}
