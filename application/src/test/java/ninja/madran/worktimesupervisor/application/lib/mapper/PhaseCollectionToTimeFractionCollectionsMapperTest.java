package ninja.madran.worktimesupervisor.application.lib.mapper;

import ninja.madran.worktimesupervisor.application.lib.interval.TimeFractionCollection;
import ninja.madran.worktimesupervisor.domain.model.Phase;
import ninja.madran.worktimesupervisor.domain.model.Task;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PhaseCollectionToTimeFractionCollectionsMapperTest {

    @Test
    public void emptyPhaseCollection() throws MapperInsufficientDataException {

        List<Phase> phases = new ArrayList<>();

        PhaseCollectionToTimeFractionCollectionsMapper mapper = new PhaseCollectionToTimeFractionCollectionsMapper();

        List<TimeFractionCollection<Task>> timeFractionCollections = mapper.map(phases);

        assertEquals(0, timeFractionCollections.size());
    }

    @Test
    public void map() throws MapperInsufficientDataException {

        Task taskA = new Task(1L, "taskA", Timestamp.from(Instant.now()), null, false);
        Task taskB = new Task(2L, "taskB", Timestamp.from(Instant.now()), null, false);
        Task taskC = new Task(3L, "taskC", Timestamp.from(Instant.now()), null, false);

        LocalDateTime startDate = LocalDateTime.of(2000, 1, 1, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2000, 1, 1, 1, 0);
        Timestamp timeNow = Timestamp.from(Instant.now());

        Phase phaseA1 = new Phase(1L, Timestamp.valueOf(startDate), Timestamp.valueOf(endDate), taskA, timeNow);
        Phase phaseA2 = new Phase(2L, Timestamp.valueOf(startDate), Timestamp.valueOf(endDate), taskA, timeNow);
        Phase phaseA3 = new Phase(3L, Timestamp.valueOf(startDate), Timestamp.valueOf(endDate), taskA, timeNow);
        Phase phaseB1 = new Phase(4L, Timestamp.valueOf(startDate), Timestamp.valueOf(endDate), taskB, timeNow);
        Phase phaseB2 = new Phase(5L, Timestamp.valueOf(startDate), Timestamp.valueOf(endDate), taskB, timeNow);
        Phase phaseC1 = new Phase(6L, Timestamp.valueOf(startDate), Timestamp.valueOf(endDate), taskC, timeNow);

        List<Phase> phases = new ArrayList<>();

        phases.add(phaseA1);
        phases.add(phaseA2);
        phases.add(phaseA3);
        phases.add(phaseB1);
        phases.add(phaseB2);
        phases.add(phaseC1);

        PhaseCollectionToTimeFractionCollectionsMapper mapper = new PhaseCollectionToTimeFractionCollectionsMapper();

        List<TimeFractionCollection<Task>> timeFractionCollections = mapper.map(phases);

        assertEquals(3, timeFractionCollections.size());

        timeFractionCollections = timeFractionCollections.stream().sorted(Comparator.comparing(o -> o.getId().id())).collect(Collectors.toList());

        assertEquals(3, timeFractionCollections.get(0).getFractions().size());
        assertEquals(2, timeFractionCollections.get(1).getFractions().size());
        assertEquals(1, timeFractionCollections.get(2).getFractions().size());
    }
}