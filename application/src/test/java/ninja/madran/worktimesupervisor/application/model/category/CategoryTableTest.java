package ninja.madran.worktimesupervisor.application.model.category;

import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.lib.test.IntegrationTest;
import ninja.madran.worktimesupervisor.application.model.project.ProjectTable;
import ninja.madran.worktimesupervisor.domain.model.Category;
import ninja.madran.worktimesupervisor.domain.model.Project;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CategoryTableTest {

    ProjectTable projectTable = (ProjectTable) ServiceContainer.INSTANCE.get(ProjectTable.class.getName());

    CategoryTable categoryTable = (CategoryTable) ServiceContainer.INSTANCE.get(CategoryTable.class.getName());

    @BeforeAll
    static void beforeAll() throws SQLException {

        IntegrationTest.buildDb();
    }

    @AfterEach
    void afterEach() throws SQLException {

        IntegrationTest.buildDb();
    }

    @Test
    public void create() throws SQLException {

        Project projectA = projectTable.find(1).orElseThrow();
        Project projectB = projectTable.find(2).orElseThrow();

        Category categoryA = new Category("Fix", projectA);
        Category categoryB = new Category("Dogs", projectB);

        categoryA = categoryTable.create(categoryA).orElseThrow();
        categoryB = categoryTable.create(categoryB).orElseThrow();

        assertEquals(3, categoryA.id());
        assertEquals("ProjectA", categoryA.project().name());
        assertEquals(4, categoryB.id());
        assertEquals("ProjectB", categoryB.project().name());
    }


    @Test
    public void find() throws SQLException {

        Category category = categoryTable.find(1).orElseThrow();

        assertEquals(1, category.id());
        assertEquals("CategoryA", category.name());
        assertEquals(1, category.project().id());
        assertEquals("ProjectA", category.project().name());
        assertEquals(false, category.isDeleted());
    }

    @Test
    public void update() throws SQLException {

        Project project = projectTable.find(2).orElseThrow();

        Category category = categoryTable.find(1).orElseThrow();

        Category newCategory = new Category(
                category.id(),
                "Update",
                project,
                category.isDeleted()
        );

        int result = categoryTable.update(newCategory);
        assertEquals(1, result);
    }

    @Test
    public void findAll() throws SQLException {

        List<Category> categories = categoryTable.findAll();

        assertEquals(2, categories.size());
        assertEquals("CategoryA", categories.get(0).name());
        assertEquals("CategoryB", categories.get(1).name());
    }

    @Test
    public void delete() throws SQLException {

        int result = categoryTable.delete(1);
        assertEquals(1, result);
    }

    @Test
    public void findByProjectId() throws SQLException {

        Project project = projectTable.find(1).orElseThrow();

        List<Category> categories = categoryTable.findAllByProjectId(project.id());

        assertEquals(2, categories.size());
        assertEquals("CategoryA", categories.get(0).name());
        assertEquals("ProjectA", categories.get(0).project().name());
    }

    @Test
    public void findByName() throws SQLException {

        Category category = categoryTable.findByName("CategoryA").orElseThrow();

        assertEquals("CategoryA", category.name());
    }
}