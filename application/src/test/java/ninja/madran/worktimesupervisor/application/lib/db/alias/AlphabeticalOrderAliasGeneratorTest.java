package ninja.madran.worktimesupervisor.application.lib.db.alias;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AlphabeticalOrderAliasGeneratorTest {

    @Test
    public void testAliases() {

        AlphabeticalOrderAliasGenerator generator = new AlphabeticalOrderAliasGenerator();

        assertEquals("a", generator.generateNextAlias());
        assertEquals("b", generator.generateNextAlias());
        assertEquals("c", generator.generateNextAlias());
        assertEquals("d", generator.generateNextAlias());
        assertEquals("e", generator.generateNextAlias());

        for (int i = 0; i < 20; i++) {
            generator.generateNextAlias();
        }

        assertEquals("z", generator.generateNextAlias());
        assertEquals("aa", generator.generateNextAlias());
        assertEquals("ba", generator.generateNextAlias());
        assertEquals("ca", generator.generateNextAlias());
        assertEquals("da", generator.generateNextAlias());
        assertEquals("ea", generator.generateNextAlias());

        for (int i = 0; i < 20; i++) {
            generator.generateNextAlias();
        }

        assertEquals("za", generator.generateNextAlias());
        assertEquals("ab", generator.generateNextAlias());
        assertEquals("bb", generator.generateNextAlias());
        assertEquals("cb", generator.generateNextAlias());
        assertEquals("db", generator.generateNextAlias());
        assertEquals("eb", generator.generateNextAlias());

        for (int i = 0; i < 643; i++) {
            generator.generateNextAlias();
        }

        assertEquals("yz", generator.generateNextAlias());
        assertEquals("zz", generator.generateNextAlias());
        assertEquals("aaa", generator.generateNextAlias());
        assertEquals("baa", generator.generateNextAlias());
    }
}