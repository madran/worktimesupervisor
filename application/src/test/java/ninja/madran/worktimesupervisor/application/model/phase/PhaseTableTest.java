package ninja.madran.worktimesupervisor.application.model.phase;

import ninja.madran.worktimesupervisor.application.lib.interval.TimeInterval;
import ninja.madran.worktimesupervisor.application.lib.mapper.MapperInsufficientDataException;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.lib.test.IntegrationTest;
import ninja.madran.worktimesupervisor.application.model.task.TaskTable;
import ninja.madran.worktimesupervisor.domain.model.Phase;
import ninja.madran.worktimesupervisor.domain.model.Task;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PhaseTableTest {

    TaskTable taskTable = (TaskTable) ServiceContainer.INSTANCE.get(TaskTable.class.getName());

    PhaseTable phaseTable = (PhaseTable) ServiceContainer.INSTANCE.get(PhaseTable.class.getName());

    @BeforeAll
    static void beforeAll() throws SQLException {

        IntegrationTest.buildDb();
    }

    @AfterEach
    void afterEach() throws SQLException {

        IntegrationTest.buildDb();
    }

    @Test
    public void create() throws SQLException {

        Task task = taskTable.find(1).orElseThrow();

        Phase phase = new Phase(
                Timestamp.from(Instant.now()),
                null,
                task
        );

        phase = phaseTable.create(phase).orElseThrow();

        assertEquals(2, phase.id());
        assertEquals(1, phase.task().id());
    }

    @Test
    public void find() throws SQLException {

        Phase phase = phaseTable.find(1).orElseThrow();

        LocalDateTime startDate = LocalDateTime.of(2000, 1, 1, 0, 0);

        assertEquals(1, phase.id());
        assertEquals(1, phase.task().id());
        assertEquals(startDate, phase.startTime().toLocalDateTime());
    }

    @Test
    void update() throws SQLException {

        Phase phase = phaseTable.find(1).orElseThrow();

        assertNull(phase.savedAt());

        Phase newPhase = new Phase(
                phase.id(),
                phase.startTime(),
                phase.endTime(),
                phase.task(),
                Timestamp.from(Instant.now())
        );

        int result = phaseTable.update(newPhase);

        assertEquals(1, result);

        phase = phaseTable.find(1).orElseThrow();

        assertNotNull(phase.savedAt());

    }

    @Test
    public void findAll() throws SQLException {

        List<Phase> phases = phaseTable.findAll();

        assertEquals(1, phases.size());
        assertEquals(1, phases.get(0).id());
    }

    @Test
    public void delete() throws SQLException {

        int result = phaseTable.delete(1);
        assertEquals(1, result);
    }

    @Test
    public void startAndEndPhase() throws SQLException {

        Task task = taskTable.find(1).orElseThrow();

        Phase phase = phaseTable.startPhase(task).orElseThrow();

        assertNotNull(phase.startTime());
        assertNull(phase.endTime());

        phaseTable.endPhase(phase);

        phase = phaseTable.find(phase.id()).orElseThrow();

        assertNotNull(phase.startTime());
        assertNotNull(phase.endTime());
    }

    @Test
    public void loadUnsavedPhases() throws SQLException {

        List<Phase> unsavedPhases = phaseTable.loadUnsavedPhases();

        assertEquals(1, unsavedPhases.size());

        Task task = taskTable.find(1).orElseThrow();

        phaseTable.startPhase(task);

        unsavedPhases = phaseTable.loadUnsavedPhases();

        assertEquals(2, unsavedPhases.size());

        for (Phase phase : unsavedPhases) {
            Phase savedPhase = new Phase(
                    phase.id(),
                    phase.startTime(),
                    phase.endTime(),
                    phase.task(),
                    Timestamp.from(Instant.now())
            );

            phaseTable.update(savedPhase);
        }

        unsavedPhases = phaseTable.loadUnsavedPhases();

        assertEquals(0, unsavedPhases.size());
    }

    @Test
    public void hasUnsavedPhases() throws SQLException {

        boolean hasUnsavedPhases = phaseTable.hasUnsavedPhases();

        assertTrue(hasUnsavedPhases);
    }

    @Test
    public void getUnsavedTasksWorkTime() throws SQLException, MapperInsufficientDataException {

        List<TimeInterval<Task>> unsavedTasksWorkTime = phaseTable.getUnsavedTasksWorkTime();

        assertEquals(1, unsavedTasksWorkTime.size());
        assertEquals(3600L, unsavedTasksWorkTime.get(0).getDuration().getSeconds());
    }
}