package ninja.madran.worktimesupervisor.application.model.project;

import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.lib.test.IntegrationTest;
import ninja.madran.worktimesupervisor.domain.model.Project;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProjectTableTest {

    ProjectTable projectTable = (ProjectTable) ServiceContainer.INSTANCE.get(ProjectTable.class.getName());

    @BeforeAll
    static void beforeAll() throws SQLException {

        IntegrationTest.buildDb();
    }

    @AfterEach
    void afterEach() throws SQLException {

        IntegrationTest.buildDb();
    }

    @Test
    public void create() throws Exception {

        Project projectA = new Project("jane", "FFFF00");

        projectA = projectTable.create(projectA).orElseThrow();
        assertEquals(3, projectA.id());
    }

    @Test
    public void find() throws Exception {

        Project projectA = projectTable.find(1).orElseThrow();

        assertEquals(1, projectA.id());
        assertEquals("ProjectA", projectA.name());
        assertEquals("00FF00", projectA.color());

        Project projectB = projectTable.find(2).orElseThrow();

        assertEquals(2, projectB.id());
        assertEquals("ProjectB", projectB.name());
        assertEquals("FF0000", projectB.color());
    }

    @Test
    public void update() throws Exception {

        Project project = projectTable.find(1).orElseThrow();

        Project newProject = new Project(
                project.id(),
                "henry",
                "00FF00"
        );

        int result = projectTable.update(newProject);
        assertEquals(1, result);
    }

    @Test
    public void findAll() throws Exception {

        List<Project> projects = projectTable.findAll();

        assertEquals(2, projects.size());
        assertEquals("ProjectA", projects.get(0).name());
        assertEquals("ProjectB", projects.get(1).name());
    }

    @Test
    public void delete() throws Exception {

        int result = projectTable.delete(1);
        assertEquals(1, result);
    }

    @Test
    public void findByProjectName() throws SQLException {

        Project project = projectTable.findByProjectName("ProjectB").orElseThrow();

        assertEquals(2, project.id());
        assertEquals("ProjectB", project.name());
    }
}