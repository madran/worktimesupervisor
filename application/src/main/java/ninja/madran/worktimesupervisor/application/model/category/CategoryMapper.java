package ninja.madran.worktimesupervisor.application.model.category;

import ninja.madran.worktimesupervisor.application.lib.db.FindMapper;
import ninja.madran.worktimesupervisor.application.lib.db.UpdateMapper;
import ninja.madran.worktimesupervisor.application.lib.db.alias.Mapper;
import ninja.madran.worktimesupervisor.domain.model.Category;
import ninja.madran.worktimesupervisor.domain.model.Project;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CategoryMapper implements Mapper<Category> {

    public FindMapper<ResultSet, Category> find() {

        return result -> new Category(
                result.getLong("id"),
                result.getString("name"),
                new Project(
                        result.getLong("project.id"),
                        result.getString("project.name"),
                        result.getString("project.color")
                ),
                result.getBoolean("is_deleted")
        );
    }

    public UpdateMapper<PreparedStatement> update(Category category) {

        return stmt -> {
            stmt.setString(1, category.name());
            stmt.setLong(2, category.project().id());
            stmt.setBoolean(3, category.isDeleted());
        };
    }
}
