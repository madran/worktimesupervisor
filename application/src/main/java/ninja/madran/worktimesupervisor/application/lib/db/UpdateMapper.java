package ninja.madran.worktimesupervisor.application.lib.db;

import java.sql.SQLException;

public interface UpdateMapper<S> {

    void execute(S stmt) throws SQLException;
}
