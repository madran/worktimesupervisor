package ninja.madran.worktimesupervisor.application.lib.db;

import ninja.madran.worktimesupervisor.application.lib.db.alias.AliasGenerator;

import java.security.InvalidParameterException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class DbTable<T> implements Table {

    protected final String TABLE_NAME;

    protected List<Table> manyToOne = new ArrayList<>();

    private final DbConnection dbConnection;

    private final AliasGenerator aliasGenerator;

    public DbTable(String tableName, DbConnection dbConnection, AliasGenerator aliasGenerator) {

        TABLE_NAME = tableName;
        this.dbConnection = dbConnection;
        this.aliasGenerator = aliasGenerator;
    }

    abstract public Optional<T> find(long id) throws SQLException;

    abstract public List<T> findAll() throws SQLException;

    abstract public Optional<T> create(T entity) throws SQLException;

    abstract public int update(T entity) throws SQLException;

    public String getTableName() {

        return TABLE_NAME;
    }

    public int delete(long id) throws InvalidParameterException, SQLException {

        if (id <= 0) {
            throw new InvalidParameterException("Id value must by higher than 0.");
        }

        String query = "delete from %s where id = ?".formatted(TABLE_NAME);

        try (
                Connection connection = getNewConnection();
                PreparedStatement stmt = connection.prepareStatement(query)
        ) {
            stmt.setLong(1, id);

            return stmt.executeUpdate();
        }
    }

    public List<String> getManyToOneRelatedTableNames() {

        List<String> list = new ArrayList<>();

        for (Table table : manyToOne) {
            list.add(table.getTableName());
            List<String> innerList = table.getManyToOneRelatedTableNames();
            list.addAll(innerList);
        }

        return list;
    }

    protected Optional<T> find(long id, FindMapper<ResultSet, T> mapper) throws SQLException {

        if (id <= 0) {
            throw new InvalidParameterException("Id value must by higher than 0.");
        }

        String mainTableAlias = aliasGenerator.generateNextAlias();

        StringBuilder query = new StringBuilder("select * from %s %s".formatted(TABLE_NAME, mainTableAlias));

        List<String> tableNames = getManyToOneRelatedTableNames();

        for (String tableName : tableNames) {
            String joinTableAlias = aliasGenerator.generateNextAlias();
            query.append(" left join %s %s on %s.id = %s"
                    .formatted(tableName, joinTableAlias, joinTableAlias, tableName));
        }

        query.append(" where %s.id = ?".formatted(mainTableAlias));


        try (
                Connection connection = getNewConnection();
                PreparedStatement stmt = connection.prepareStatement(query.toString())
        ) {
            stmt.setLong(1, id);

            try (ResultSet result = stmt.executeQuery()) {

                if (result.next()) {
                    return Optional.of(mapper.execute(result));
                }

                return Optional.empty();
            }

        }
    }

    protected List<T> findAll(FindMapper<ResultSet, T> mapper) throws SQLException {

        String mainTableAlias = aliasGenerator.generateNextAlias();

        StringBuilder query = new StringBuilder("select * from %s %s".formatted(TABLE_NAME, mainTableAlias));

        List<String> tableNames = getManyToOneRelatedTableNames();

        for (String tableName : tableNames) {
            String joinTableAlias = aliasGenerator.generateNextAlias();
            query.append(" left join %s %s on %s.id = %s.id"
                    .formatted(tableName, joinTableAlias, joinTableAlias, mainTableAlias));
        }

        List<T> entities = new ArrayList<>();

        try (
                Connection connection = getNewConnection();
                Statement stmt = connection.createStatement();
                ResultSet result = stmt.executeQuery(query.toString())
        ) {
            while (result.next()) {
                T entity = mapper.execute(result);

                entities.add(entity);
            }

            return entities;
        }
    }

    protected Optional<T> create(List<String> fields, UpdateMapper<PreparedStatement> mapper) throws SQLException {

        String fieldList = String.join(",", fields);
        String placeholders = fields.stream().map(s -> "?").collect(Collectors.joining(","));

        String query = "insert into %s (%s) values (%s)"
                .formatted(TABLE_NAME, fieldList, placeholders);

        try (
                Connection connection = getNewConnection();
                PreparedStatement stmt = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)
        ) {
            mapper.execute(stmt);

            int affectedRows = stmt.executeUpdate();

            if (affectedRows == 0) {
                return Optional.empty();
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return find(generatedKeys.getLong(1));
                }
            }

            return Optional.empty();
        }
    }

    protected int update(long id, List<String> fields, UpdateMapper<PreparedStatement> mapper) throws SQLException {

        if (id <= 0) {
            throw new InvalidParameterException("Id value must by higher than 0.");
        }

        String fieldList = fields.stream().map(s -> s + " = ?").collect(Collectors.joining(","));

        String query = "update %s set %s where id = ?".formatted(TABLE_NAME, fieldList);

        try (
                Connection connection = getNewConnection();
                PreparedStatement stmt = connection.prepareStatement(query)
        ) {

            mapper.execute(stmt);

            stmt.setLong(fields.size() + 1, id);

            return stmt.executeUpdate();
        }
    }

    protected Connection getNewConnection() throws SQLException {

        return dbConnection.createConnection();
    }
}
