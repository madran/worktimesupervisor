package ninja.madran.worktimesupervisor.application.config;

import ninja.madran.worktimesupervisor.application.lib.db.DbConnection;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class CreateTables {

    private static final String project = "project";
    private static final String category = "category";
    private static final String task = "task";
    private static final String phase = "phase";

    private static final Map<String, String> sql = new LinkedHashMap<>();

    public static void create(DbConnection dbConnection) throws SQLException {

        prepareSQL();

        Connection connection = dbConnection.createConnection();
        connection.setAutoCommit(false);

        Statement stmt = connection.createStatement();

        for (Map.Entry<String, String> entry : sql.entrySet()) {
            String dropProjectTableSQL = "drop table if exists %s cascade".formatted(entry.getKey());

            stmt.execute(dropProjectTableSQL);

            String createTableSQL = entry.getValue();

            stmt.execute(createTableSQL);
        }

        stmt.execute("create unique index \"%s-unique-name\" on %s (name, %s)".formatted(task, task, category));

        connection.commit();
        connection.close();
    }

    private static void prepareSQL() {

        sql.put(
                project,
                """
                        create table %s(
                            id bigint primary key auto_increment not null,
                            name varchar(100) unique not null,
                            color varchar(6) unique not null
                        )
                        """.formatted(project)
        );

        sql.put(
                category,
                """
                        create table %s(
                            id bigint primary key auto_increment not null,
                            name varchar(100) unique not null,
                            %s bigint not null,
                            is_deleted bool not null default false,
                            foreign key (%s) references %s (id) on delete cascade
                        )
                        """.formatted(category, project, project, project)
        );

        sql.put(
                task,
                """
                        create table %s(
                            id bigint primary key auto_increment not null,
                            name varchar(250) not null,
                            time_of_last_use timestamp,
                            %s bigint not null,
                            is_deleted bool not null default false,
                            foreign key (%s) references %s (id) on delete cascade
                        )
                        """.formatted(task, category, category, category)
        );

        sql.put(
                phase,
                """
                        create table %s(
                            id bigint primary key auto_increment not null,
                            start_time timestamp not null,
                            end_time timestamp,
                            %s bigint not null,
                            saved_at timestamp,
                            foreign key (%s) references %s (id) on delete cascade
                        )
                        """.formatted(phase, task, task, task)
        );
    }
}
