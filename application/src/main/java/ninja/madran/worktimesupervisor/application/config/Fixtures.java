package ninja.madran.worktimesupervisor.application.config;

import ninja.madran.worktimesupervisor.application.lib.db.DbConnection;

import java.sql.*;
import java.time.Instant;
import java.time.LocalDateTime;

public abstract class Fixtures {

    public static void create(DbConnection dbConnection) throws SQLException {

        Connection connection = dbConnection.createConnection();
        Statement stmt = connection.createStatement();

        //Projects
        stmt.execute("insert into project (name, color) values ('ProjectA', '00FF00')");
        stmt.execute("insert into project (name, color) values ('ProjectB', 'FF0000')");

        //Categories
        stmt.execute("insert into category (name, project) values('CategoryA', 1)");
        stmt.execute("insert into category (name, project) values('CategoryB', 1)");

        //Tasks for category projectA
        PreparedStatement prepStmt = connection.prepareStatement(
                "insert into task (name, time_of_last_use, category) values ('update', ?, 1)"
        );
        prepStmt.setTimestamp(1, Timestamp.from(Instant.now()));

        prepStmt.execute();

        prepStmt = connection.prepareStatement(
                "insert into task (name, time_of_last_use, category) values ('delete', ?, 1)"
        );
        prepStmt.setTimestamp(1, Timestamp.from(Instant.now()));

        prepStmt.execute();

        prepStmt = connection.prepareStatement(
                "insert into task (name, time_of_last_use, category) values ('create', ?, 1)"
        );
        prepStmt.setTimestamp(1, Timestamp.from(Instant.now()));

        prepStmt.execute();

        prepStmt = connection.prepareStatement(
                "insert into task (name, time_of_last_use, category) values ('This is very long task name. This is very long task name. This is very long task name, name. This is very long task name. This is very long task name. This is very long task name. This is very long task name, name.', ?, 1)"
        );
        prepStmt.setTimestamp(1, Timestamp.from(Instant.now()));

        prepStmt.execute();

        //Tasks for category projectB
        prepStmt = connection.prepareStatement(
                "insert into task (name, time_of_last_use, category) values ('new', ?, 2)"
        );
        prepStmt.setTimestamp(1, Timestamp.from(Instant.now()));

        prepStmt.execute();

        prepStmt = connection.prepareStatement(
                "insert into task (name, time_of_last_use, category) values ('add', ?, 2)"
        );
        prepStmt.setTimestamp(1, Timestamp.from(Instant.now()));

        prepStmt.execute();

        prepStmt = connection.prepareStatement(
                "insert into task (name, time_of_last_use, category) values ('move', ?, 2)"
        );
        prepStmt.setTimestamp(1, Timestamp.from(Instant.now()));

        prepStmt.execute();

        prepStmt = connection.prepareStatement(
                "insert into phase (start_time, end_time, task) values (?, ?, 1)"
        );

        LocalDateTime startDate = LocalDateTime.of(2000, 1, 1, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2000, 1, 1, 1, 0);

        prepStmt.setTimestamp(1, Timestamp.valueOf(startDate));
        prepStmt.setTimestamp(2, Timestamp.valueOf(endDate));

        prepStmt.execute();
    }
}
