package ninja.madran.worktimesupervisor.application.config;

import ninja.madran.worktimesupervisor.application.lib.detector.Detector;
import ninja.madran.worktimesupervisor.application.lib.detector.OSDetector;

public class AppDevConfig extends AppConfig {

    private final String windowsAppFilesPath;

    private final String unixAppFilesPath;

    public AppDevConfig(Detector<OSDetector.OS> osDetector) {
        super(osDetector);

        windowsAppFilesPath = System.getenv("LOCALAPPDATA") + "/" + packageName + "/" + appName;

        unixAppFilesPath = System.getProperty("user.home") + "/." + packageName + "/" + appName;
    }

    @Override
    public String getDbUrl() {

        return switch (os) {
            case WINDOWS -> "jdbc:h2:" + windowsAppFilesPath;
            case UNIX -> "jdbc:h2:" + unixAppFilesPath;
            default -> "jdbc:h2:";
        };
    }
}
