package ninja.madran.worktimesupervisor.application.lib.mapper;

import ninja.madran.worktimesupervisor.application.lib.interval.TimeFraction;
import ninja.madran.worktimesupervisor.application.lib.interval.TimeFractionCollection;
import ninja.madran.worktimesupervisor.domain.model.Phase;
import ninja.madran.worktimesupervisor.domain.model.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PhaseCollectionToTimeFractionCollectionsMapper {

    public List<TimeFractionCollection<Task>> map(List<Phase> phases) throws MapperInsufficientDataException {

        List<Task> tasks = phases.stream().map(Phase::task).distinct().collect(Collectors.toList());

        Map<Task, TimeFractionCollection<Task>> timeFractionCollections = new HashMap<>();

        for (Task task : tasks) {
            timeFractionCollections.put(task, new TimeFractionCollection<>(task));
        }

        for (Phase phase : phases) {

            if (phase.endTime() == null) {
                throw new MapperInsufficientDataException("Phase end time not provided");
            }

            TimeFraction fraction = new TimeFraction(phase.startTime(), phase.endTime());

            timeFractionCollections.get(phase.task()).add(fraction);
        }

        return new ArrayList<>(timeFractionCollections.values());
    }
}
