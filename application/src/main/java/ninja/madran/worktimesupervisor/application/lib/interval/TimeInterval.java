package ninja.madran.worktimesupervisor.application.lib.interval;

import java.time.Duration;

public class TimeInterval<T> {

    private final T id;

    private final Duration duration;

    public TimeInterval(T id, Duration duration) {

        this.id = id;
        this.duration = duration;
    }

    public T getId() {

        return id;
    }

    public Duration getDuration() {

        return duration;
    }
}
