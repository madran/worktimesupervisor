package ninja.madran.worktimesupervisor.application.model.phase;

import ninja.madran.worktimesupervisor.application.lib.db.FindMapper;
import ninja.madran.worktimesupervisor.application.lib.db.UpdateMapper;
import ninja.madran.worktimesupervisor.application.lib.db.alias.Mapper;
import ninja.madran.worktimesupervisor.domain.model.Category;
import ninja.madran.worktimesupervisor.domain.model.Task;
import ninja.madran.worktimesupervisor.domain.model.Phase;
import ninja.madran.worktimesupervisor.domain.model.Project;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class PhaseMapper implements Mapper<Phase> {

    @Override
    public FindMapper<ResultSet, Phase> find() {

        return result -> new Phase(
                result.getLong("id"),
                result.getTimestamp("start_time"),
                result.getTimestamp("end_time"),
                new Task(
                        result.getLong("task.id"),
                        result.getString("task.name"),
                        result.getTimestamp("task.time_of_last_use"),
                        new Category(
                                result.getLong("category.id"),
                                result.getString("category.name"),
                                new Project(
                                        result.getLong("project.id"),
                                        result.getString("project.name"),
                                        result.getString("project.color")
                                ),
                                result.getBoolean("category.is_deleted")
                        ),
                        result.getBoolean("task.is_deleted")
                ),
                result.getTimestamp("saved_at")
        );
    }

    @Override
    public UpdateMapper<PreparedStatement> update(Phase entity) {

        return stmt -> {
            stmt.setTimestamp(1, entity.startTime());
            stmt.setTimestamp(2, entity.endTime());
            stmt.setLong(3, entity.task().id());
            stmt.setTimestamp(4, entity.savedAt());
        };
    }
}
