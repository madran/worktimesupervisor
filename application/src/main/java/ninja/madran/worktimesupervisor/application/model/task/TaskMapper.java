package ninja.madran.worktimesupervisor.application.model.task;

import ninja.madran.worktimesupervisor.application.lib.db.FindMapper;
import ninja.madran.worktimesupervisor.application.lib.db.UpdateMapper;
import ninja.madran.worktimesupervisor.application.lib.db.alias.Mapper;
import ninja.madran.worktimesupervisor.domain.model.Category;
import ninja.madran.worktimesupervisor.domain.model.Project;
import ninja.madran.worktimesupervisor.domain.model.Task;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class TaskMapper implements Mapper<Task> {

    public FindMapper<ResultSet, Task> find() {
        return result -> new Task(
                result.getLong("id"),
                result.getString("name"),
                result.getTimestamp("time_of_last_use"),
                new Category(
                        result.getLong("category.id"),
                        result.getString("category.name"),
                        new Project(
                                result.getLong("project.id"),
                                result.getString("project.name"),
                                result.getString("project.color")
                        ),
                        result.getBoolean("category.is_deleted")
                ),
                result.getBoolean("is_deleted")
        );
    }

    public UpdateMapper<PreparedStatement> update(Task task) {

        return stmt -> {
            stmt.setString(1, task.name());
            stmt.setTimestamp(2, task.timeOfLastUse());
            stmt.setLong(3, task.category().id());
            stmt.setBoolean(4, task.isDeleted());
        };
    }
}
