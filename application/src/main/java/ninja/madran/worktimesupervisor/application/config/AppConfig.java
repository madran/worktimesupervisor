package ninja.madran.worktimesupervisor.application.config;

import ninja.madran.worktimesupervisor.application.lib.detector.Detector;
import ninja.madran.worktimesupervisor.application.lib.detector.OSDetector;


public abstract class AppConfig implements DbConfig {

    protected final String appName;

    protected final String packageName;

    protected final String dbFileName;

    protected OSDetector.OS os = OSDetector.OS.INVALID_SYSTEM;

    public AppConfig() {

        appName = "WorkTimeSupervisor";

        packageName = "ninjamadran";

        dbFileName = "worktimesupervisor.h2.db";
    }

    public AppConfig(Detector<OSDetector.OS> osDetector) {

        appName = "WorkTimeSupervisor";

        packageName = "ninjamadran";

        dbFileName = "worktimesupervisor.h2.db";

        os = osDetector.detect();
    }


}
