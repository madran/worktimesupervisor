package ninja.madran.worktimesupervisor.application.lib.db;

import java.util.List;

public interface Table {

    List<String> getManyToOneRelatedTableNames();

    String getTableName();
}
