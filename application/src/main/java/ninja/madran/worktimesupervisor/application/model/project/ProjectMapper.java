package ninja.madran.worktimesupervisor.application.model.project;

import ninja.madran.worktimesupervisor.application.lib.db.FindMapper;
import ninja.madran.worktimesupervisor.application.lib.db.UpdateMapper;
import ninja.madran.worktimesupervisor.application.lib.db.alias.Mapper;
import ninja.madran.worktimesupervisor.domain.model.Project;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ProjectMapper implements Mapper<Project> {

    public FindMapper<ResultSet, Project> find() {

        return result -> new Project(
                result.getLong("id"),
                result.getString("name"),
                result.getString("color")
        );
    }

    public UpdateMapper<PreparedStatement> update(Project project) {

        return stmt -> {
            stmt.setString(1, project.name());
            stmt.setString(2, project.color());
        };
    }
}
