package ninja.madran.worktimesupervisor.application.lib.db.alias;

public class AlphabeticalOrderAliasGenerator implements AliasGenerator {

    private final char[] characters = new char[16];

    private int length = 1;

    public AlphabeticalOrderAliasGenerator() {

        for (int i = 0; i < 16; i++) {
            characters[i] = 96;
        }
    }

    public String generateNextAlias() {

        if (characters[15] == 122) {
            throw new AliasGeneratorException("The maximum number of aliases has been reached.");
        }

        generate(0);

        return String.valueOf(characters).substring(0, length);
    }

    private void generate(int i) {

        if (characters[i] == 122) {
            characters[i] = 97;
            i++;
            if (length < i + 1) {
                length = i + 1;
            }
            generate(i);
        } else {
            characters[i]++;
        }
    }
}
