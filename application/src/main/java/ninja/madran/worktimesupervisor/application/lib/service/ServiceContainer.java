package ninja.madran.worktimesupervisor.application.lib.service;

import java.util.HashMap;
import java.util.Map;

public enum ServiceContainer {

    INSTANCE;

    private final static Map<String, Object> registry = new HashMap<>();

    public void add(String name, Object object) {

        registry.put(name, object);
    }

    public Object get(String name) {

        return registry.get(name);
    }
}
