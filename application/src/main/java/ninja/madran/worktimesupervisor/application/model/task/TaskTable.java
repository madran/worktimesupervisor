package ninja.madran.worktimesupervisor.application.model.task;

import ninja.madran.worktimesupervisor.application.lib.db.DbConnection;
import ninja.madran.worktimesupervisor.application.lib.db.DbTable;
import ninja.madran.worktimesupervisor.application.lib.db.alias.AlphabeticalOrderAliasGenerator;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.model.category.CategoryTable;
import ninja.madran.worktimesupervisor.domain.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TaskTable extends DbTable<Task> {

    private final TaskMapper mapper;

    private final List<String> fields = new ArrayList<>();

    public TaskTable(DbConnection dbConnection, CategoryTable categoryTable, TaskMapper mapper) {

        super("task", dbConnection, new AlphabeticalOrderAliasGenerator());
        this.mapper = mapper;

        fields.add("name");
        fields.add("time_of_last_use");
        fields.add(categoryTable.getTableName());
        fields.add("is_deleted");

        super.manyToOne.add(categoryTable);
    }

    @Override
    public Optional<Task> find(long id) throws SQLException {

        return find(id, mapper.find());
    }

    @Override
    public List<Task> findAll() throws SQLException {

        return findAll(mapper.find());
    }

    @Override
    public Optional<Task> create(Task task) throws SQLException {

        return create(fields, mapper.update(task));
    }

    @Override
    public int update(Task task) throws SQLException {

        return update(task.id(), fields, mapper.update(task));
    }

    public List<Task> findAllRecentTasksByCategoryId(long id, int limit) throws SQLException {

        List<Task> tasks = new ArrayList<>();

        try (
                Connection connection = getNewConnection();
                PreparedStatement stmt = connection.prepareStatement(
                        """
                                    select * from %s t
                                    join category c on t.category = c.id
                                    join project u on c.project = u.id
                                    where c.id = ?
                                    order by t.time_of_last_use desc
                                    limit ?
                                """.formatted(getTableName())
                )
        ) {
            stmt.setLong(1, id);
            stmt.setLong(2, limit);

            try (ResultSet result = stmt.executeQuery()) {
                while (result.next()) {
                    Task task = mapper.find().execute(result);
                    tasks.add(task);
                }
            }
        }

        return tasks;
    }
}
