package ninja.madran.worktimesupervisor.application.lib.test;

import ninja.madran.worktimesupervisor.application.config.*;
import ninja.madran.worktimesupervisor.application.lib.db.DbConnection;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;

import java.sql.SQLException;


public abstract class IntegrationTest {

    public static void buildDb() throws SQLException {

        ServiceContainer services = ServiceContainer.INSTANCE;

        AppConfig config = new AppTestConfig();
        Services.load(config, services);
        CreateTables.create((DbConnection) services.get(DbConnection.class.getName()));
        Fixtures.create((DbConnection) services.get(DbConnection.class.getName()));
    }
}
