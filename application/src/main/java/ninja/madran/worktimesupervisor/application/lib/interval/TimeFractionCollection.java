package ninja.madran.worktimesupervisor.application.lib.interval;

import java.util.ArrayList;
import java.util.List;

public class TimeFractionCollection<T> {

    private final T id;

    private final List<TimeFraction> timeFractions;

    public TimeFractionCollection(T id) {

        this.id = id;
        this.timeFractions = new ArrayList<>();
    }

    public boolean add(TimeFraction timeFraction) {

        return timeFractions.add(timeFraction);
    }

    public T getId() {

        return id;
    }

    public List<TimeFraction> getFractions() {

        return timeFractions;
    }
}
