package ninja.madran.worktimesupervisor.application.api;

import java.util.List;

public interface Client {

    List<String> loadTasks(String name);
}
