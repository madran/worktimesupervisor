package ninja.madran.worktimesupervisor.application.lib.db;

import ninja.madran.worktimesupervisor.application.config.DbConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {

    private final String dbUrl;

    public DbConnection(DbConfig config) {

        dbUrl = config.getDbUrl();
    }

    public Connection createConnection() throws SQLException {

        return DriverManager.getConnection(dbUrl);
    }
}
