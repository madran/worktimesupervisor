package ninja.madran.worktimesupervisor.application.lib.db.alias;

import ninja.madran.worktimesupervisor.application.lib.db.FindMapper;
import ninja.madran.worktimesupervisor.application.lib.db.UpdateMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public interface Mapper<T> {

    FindMapper<ResultSet, T> find();

    UpdateMapper<PreparedStatement> update(T entity);
}
