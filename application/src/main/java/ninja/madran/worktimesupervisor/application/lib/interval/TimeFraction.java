package ninja.madran.worktimesupervisor.application.lib.interval;

import java.sql.Timestamp;

public class TimeFraction {

    private final Timestamp start;

    private final Timestamp stop;

    public TimeFraction(Timestamp start, Timestamp stop) {

        this.start = start;
        this.stop = stop;
    }

    public Timestamp getStart() {

        return start;
    }

    public Timestamp getStop() {

        return stop;
    }
}
