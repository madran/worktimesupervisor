package ninja.madran.worktimesupervisor.application.model.phase;

import ninja.madran.worktimesupervisor.application.lib.db.DbConnection;
import ninja.madran.worktimesupervisor.application.lib.db.DbTable;
import ninja.madran.worktimesupervisor.application.lib.db.alias.AlphabeticalOrderAliasGenerator;
import ninja.madran.worktimesupervisor.application.lib.interval.SimpleFractionAdder;
import ninja.madran.worktimesupervisor.application.lib.interval.TimeFractionCollection;
import ninja.madran.worktimesupervisor.application.lib.interval.TimeInterval;
import ninja.madran.worktimesupervisor.application.lib.mapper.MapperInsufficientDataException;
import ninja.madran.worktimesupervisor.application.lib.mapper.PhaseCollectionToTimeFractionCollectionsMapper;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.model.task.TaskTable;
import ninja.madran.worktimesupervisor.domain.model.Task;
import ninja.madran.worktimesupervisor.domain.model.Phase;

import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PhaseTable extends DbTable<Phase> {

    private final PhaseMapper mapper;

    private final List<String> fields = new ArrayList<>();

    private final String SQL_UNSAVED_PHASES = "from phase p join task t on p.task = t.id join category c on t.category = c.id join project u on c.project = u.id where saved_at is null";

    public PhaseTable(DbConnection dbConnection, TaskTable taskTable, PhaseMapper mapper) {

        super("phase", dbConnection, new AlphabeticalOrderAliasGenerator());
        this.mapper = mapper;

        fields.add("start_time");
        fields.add("end_time");
        fields.add("task");
        fields.add("saved_at");

        super.manyToOne.add(taskTable);
    }

    public Optional<Phase> startPhase(Task task) throws SQLException {

        Phase phase = new Phase(Timestamp.from(Instant.now()), null, task);
        return create(phase);
    }

    public void endPhase(Phase phase) throws SQLException {

        Phase newPhase = new Phase(
                phase.id(),
                phase.startTime(),
                Timestamp.from(Instant.now()),
                phase.task(),
                phase.savedAt()
        );

        update(newPhase);
    }

    public List<Phase> loadUnsavedPhases() throws SQLException {

        try (
                Connection connection = getNewConnection();
                Statement stmt = connection.createStatement();
                ResultSet result = stmt.executeQuery(
                        "select * " + SQL_UNSAVED_PHASES
                )
        ) {
            List<Phase> phases = new ArrayList<>();

            while (result.next()) {
                Phase phase = mapper.find().execute(result);
                phases.add(phase);
            }

            return phases;
        }
    }

    public boolean hasUnsavedPhases() throws SQLException {

        try (
                Connection connection = getNewConnection();
                Statement stmt = connection.createStatement();
                ResultSet result = stmt.executeQuery(
                        "select count(*) " + SQL_UNSAVED_PHASES
                )
        ) {

            result.next();
            int numberOfUnsavedPhases = result.getInt(1);

            return numberOfUnsavedPhases > 0;
        }
    }

    public List<TimeInterval<Task>> getUnsavedTasksWorkTime() throws SQLException, MapperInsufficientDataException {

        List<Phase> phases = loadUnsavedPhases();

        PhaseCollectionToTimeFractionCollectionsMapper mapper = new PhaseCollectionToTimeFractionCollectionsMapper();

        SimpleFractionAdder<Task> fractionAdder = new SimpleFractionAdder<>();

        List<TimeInterval<Task>> intervals = new ArrayList<>();

        for (TimeFractionCollection<Task> fractionCollection : mapper.map(phases)) {
            intervals.add(fractionAdder.sum(fractionCollection));
        }

        return intervals;
    }

    @Override
    public Optional<Phase> find(long id) throws SQLException {

        return find(id, mapper.find());
    }

    @Override
    public List<Phase> findAll() throws SQLException {

        return findAll(mapper.find());
    }

    @Override
    public Optional<Phase> create(Phase entity) throws SQLException {

        return create(fields, mapper.update(entity));
    }

    @Override
    public int update(Phase entity) throws SQLException {

        return update(entity.id(), fields, mapper.update(entity));
    }
}
