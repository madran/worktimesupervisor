package ninja.madran.worktimesupervisor.application.config;

import java.io.File;
import java.net.URISyntaxException;

public class AppTestConfig extends AppConfig {

    //jdbc:h2:%s;DATABASE_TO_UPPER=false;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
    @Override
    public String getDbUrl() {

        String path = "";
        try {
            path = new File(AppTestConfig.class.getProtectionDomain().getCodeSource().getLocation()
                    .toURI()).getPath();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        path = path.replace("\\", "/");

        path += "/../../db/test";

        return "jdbc:h2:%s;DATABASE_TO_UPPER=true;USER=test;PASSWORD=test"
                .formatted(path);
    }
}
