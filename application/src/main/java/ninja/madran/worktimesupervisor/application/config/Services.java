package ninja.madran.worktimesupervisor.application.config;

import ninja.madran.worktimesupervisor.application.api.ClientJira;
import ninja.madran.worktimesupervisor.application.lib.db.DbConnection;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.model.category.CategoryMapper;
import ninja.madran.worktimesupervisor.application.model.category.CategoryTable;
import ninja.madran.worktimesupervisor.application.model.phase.PhaseMapper;
import ninja.madran.worktimesupervisor.application.model.phase.PhaseTable;
import ninja.madran.worktimesupervisor.application.model.task.TaskMapper;
import ninja.madran.worktimesupervisor.application.model.task.TaskTable;
import ninja.madran.worktimesupervisor.application.model.project.ProjectMapper;
import ninja.madran.worktimesupervisor.application.model.project.ProjectTable;

public class Services {

    public static void load(AppConfig config, ServiceContainer services) {

        services.add(AppConfig.class.getName(), config);

        services.add(
                DbConnection.class.getName(),
                new DbConnection(
                        (AppConfig) services.get(AppConfig.class.getName())
                )
        );

        services.add(
                ProjectTable.class.getName(),
                new ProjectTable(
                        (DbConnection) services.get(DbConnection.class.getName()),
                        new ProjectMapper()
                )
        );

        services.add(
                CategoryTable.class.getName(),
                new CategoryTable(
                        (DbConnection) services.get(DbConnection.class.getName()),
                        (ProjectTable) services.get(ProjectTable.class.getName()),
                        new CategoryMapper()
                )
        );

        services.add(
                TaskTable.class.getName(),
                new TaskTable(
                        (DbConnection) services.get(DbConnection.class.getName()),
                        (CategoryTable) services.get(CategoryTable.class.getName()),
                        new TaskMapper()

                )
        );

        services.add(
                PhaseTable.class.getName(),
                new PhaseTable(
                        (DbConnection) services.get(DbConnection.class.getName()),
                        (TaskTable) services.get(TaskTable.class.getName()),
                        new PhaseMapper()
                )
        );

        services.add(
                ClientJira.class.getName(),
                new ClientJira()
        );
    }
}
