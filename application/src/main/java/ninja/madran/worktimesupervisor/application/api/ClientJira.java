package ninja.madran.worktimesupervisor.application.api;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ClientJira implements Client {

    @Override
    public List<String> loadTasks(String name) {

        List<String> taskNames = new ArrayList<>();
        taskNames.add("fjelds");
        taskNames.add("fjeld");
        taskNames.add("jaker");
        taskNames.add("jakes");
        taskNames.add("jarks");
        taskNames.add("jerks");
        taskNames.add("jake");
        taskNames.add("jaks");
        taskNames.add("jark");
        taskNames.add("jerk");
        taskNames.add("flakers");
        taskNames.add("flaked");
        taskNames.add("fakers");
        taskNames.add("flaker");
        taskNames.add("flakes");
        taskNames.add("freaks");
        taskNames.add("faked");
        taskNames.add("jades");
        taskNames.add("faker");
        taskNames.add("fakes");
        taskNames.add("flake");
        taskNames.add("flaks");
        taskNames.add("flask");
        taskNames.add("freak");
        taskNames.add("jarls");
        taskNames.add("kerfs");
        taskNames.add("rajes");
        taskNames.add("skelf");
        taskNames.add("jade");
        taskNames.add("fardels");
        taskNames.add("dakers");
        taskNames.add("darkle");
        taskNames.add("drakes");
        taskNames.add("larked");
        taskNames.add("slaked");
        taskNames.add("fake");
        taskNames.add("flak");
        taskNames.add("jars");
        taskNames.add("kafs");
        taskNames.add("kefs");
        taskNames.add("kerf");
        taskNames.add("faders");
        taskNames.add("falsed");
        taskNames.add("fardel");
        taskNames.add("farsed");
        taskNames.add("flared");
        taskNames.add("lakers");
        taskNames.add("slaker");
        taskNames.add("arked");
        taskNames.add("asked");
        taskNames.add("daker");
        taskNames.add("darks");
        taskNames.add("drake");
        taskNames.add("dreks");
        taskNames.add("kades");
        taskNames.add("laked");


        return taskNames.stream().filter(s -> Pattern.matches("^" + name + ".*", s)).collect(Collectors.toList());
    }
}
