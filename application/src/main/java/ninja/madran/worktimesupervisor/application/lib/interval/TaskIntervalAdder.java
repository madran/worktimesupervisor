package ninja.madran.worktimesupervisor.application.lib.interval;

import ninja.madran.worktimesupervisor.domain.model.Phase;

import java.sql.Timestamp;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskIntervalAdder {

    private final List<Phase> phases;

    public TaskIntervalAdder(List<Phase> phases) {

        this.phases = phases;
    }

    public Map<Long, Duration> getTimeIntervals() {

        Map<Long, Duration> taskDurations = new HashMap<>();

        for (Phase phase : phases) {
            taskDurations.put(phase.id(), Duration.ofSeconds(0));
        }

        for (Phase phase : phases) {

            Timestamp start = phase.startTime();
            Timestamp end = phase.endTime();

            Duration duration = Duration.between(start.toLocalDateTime(), end.toLocalDateTime());

            Duration durationSum = taskDurations.get(phase.id());

            taskDurations.put(phase.id(), durationSum.plus(duration));
        }

        return taskDurations;
    }
}
