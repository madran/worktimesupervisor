package ninja.madran.worktimesupervisor.application.lib.mapper;

public class MapperInsufficientDataException extends Exception {

    public MapperInsufficientDataException(String message) {

        super(message);
    }
}
