package ninja.madran.worktimesupervisor.application.lib.detector;

public class OSDetector implements Detector<OSDetector.OS> {

    public enum OS {
        WINDOWS, UNIX, INVALID_SYSTEM
    }

    public OSDetector.OS detect() {

        String osName = System.getProperty("os.name").toLowerCase();

        if (isWindows(osName)) {
            return OS.WINDOWS;
        } else if (isUnix(osName)) {
            return OS.UNIX;
        } else {
            return OS.INVALID_SYSTEM;
        }
    }

    private static boolean isWindows(String osName) {

        return (osName.contains("win"));
    }

    private static boolean isUnix(String osName) {

        return (osName.contains("nux"));
    }
}
