package ninja.madran.worktimesupervisor.application.lib.detector;

public interface Detector<T> {

    T detect();
}
