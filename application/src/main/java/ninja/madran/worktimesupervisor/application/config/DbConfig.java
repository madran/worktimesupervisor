package ninja.madran.worktimesupervisor.application.config;

public interface DbConfig {

    String getDbUrl();
}
