package ninja.madran.worktimesupervisor.application.lib.interval;

import java.time.Duration;

public class SimpleFractionAdder<T> {

    public TimeInterval<T> sum(TimeFractionCollection<T> timeFractionCollection) {

        Duration durationSum = Duration.ZERO;

        for (TimeFraction timeFraction : timeFractionCollection.getFractions()) {
            Duration duration = Duration.between(
                    timeFraction.getStart().toLocalDateTime(),
                    timeFraction.getStop().toLocalDateTime()
            );

            durationSum = durationSum.plus(duration);
        }

        return new TimeInterval<>(timeFractionCollection.getId(), durationSum);
    }
}
