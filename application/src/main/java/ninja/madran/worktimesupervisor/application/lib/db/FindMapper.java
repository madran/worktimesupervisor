package ninja.madran.worktimesupervisor.application.lib.db;

import java.sql.SQLException;

public interface FindMapper<I, R> {

    R execute(I input) throws SQLException;
}
