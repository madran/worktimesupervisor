package ninja.madran.worktimesupervisor.application.lib.db.alias;

public interface AliasGenerator {

    String generateNextAlias();
}
