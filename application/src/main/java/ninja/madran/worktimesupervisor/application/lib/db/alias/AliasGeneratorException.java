package ninja.madran.worktimesupervisor.application.lib.db.alias;

public class AliasGeneratorException extends RuntimeException {

    public AliasGeneratorException(String message) {

        super(message);
    }
}
