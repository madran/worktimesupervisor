package ninja.madran.worktimesupervisor.application.model.category;

import ninja.madran.worktimesupervisor.application.lib.db.DbConnection;
import ninja.madran.worktimesupervisor.application.lib.db.DbTable;
import ninja.madran.worktimesupervisor.application.lib.db.alias.AlphabeticalOrderAliasGenerator;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.model.project.ProjectTable;
import ninja.madran.worktimesupervisor.domain.model.Category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CategoryTable extends DbTable<Category> {

    private final CategoryMapper mapper;

    private final List<String> fields = new ArrayList<>();

    public CategoryTable(DbConnection dbConnection, ProjectTable projectTable, CategoryMapper mapper) {

        super("category", dbConnection, new AlphabeticalOrderAliasGenerator());
        this.mapper = mapper;

        fields.add("name");
        fields.add(projectTable.getTableName());
        fields.add("is_deleted");

        super.manyToOne.add(projectTable);
    }

    @Override
    public Optional<Category> find(long id) throws SQLException {

        return super.find(id, mapper.find());
    }

    @Override
    public List<Category> findAll() throws SQLException {

        return super.findAll(mapper.find());
    }

    @Override
    public Optional<Category> create(Category category) throws SQLException {

        return super.create(fields, mapper.update(category));
    }

    @Override
    public int update(Category category) throws SQLException {

        return super.update(category.id(), fields, mapper.update(category));
    }

    public List<Category> findAllByProjectId(long projectId) throws SQLException {

        List<Category> categories = new ArrayList<>();

        try (
                Connection connection = getNewConnection();
                PreparedStatement stmt = connection.prepareStatement(
                        "select * from %s c join project u on c.project = u.id where u.id = ?".formatted(getTableName())
                )
        ) {
            stmt.setLong(1, projectId);

            try (ResultSet result = stmt.executeQuery()) {
                while (result.next()) {
                    Category category = mapper.find().execute(result);
                    categories.add(category);
                }
            }
        }

        return categories;
    }

    public Optional<Category> findByName(String name) throws SQLException {

        try (
                Connection connection = getNewConnection();
                PreparedStatement stmt = connection.prepareStatement(
                        "select * from %s c join project u on c.project = u.id where c.name = ?".formatted(getTableName())
                )
        ) {

            stmt.setString(1, name);

            try (ResultSet result = stmt.executeQuery()) {
                if (result.next()) {
                    return Optional.of(mapper.find().execute(result));
                }
            }
        }

        return Optional.empty();
    }
}
