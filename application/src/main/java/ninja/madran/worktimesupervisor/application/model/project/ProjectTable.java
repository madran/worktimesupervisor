package ninja.madran.worktimesupervisor.application.model.project;

import ninja.madran.worktimesupervisor.application.lib.db.DbConnection;
import ninja.madran.worktimesupervisor.application.lib.db.DbTable;
import ninja.madran.worktimesupervisor.application.lib.db.alias.AlphabeticalOrderAliasGenerator;
import ninja.madran.worktimesupervisor.domain.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProjectTable extends DbTable<Project> {

    private final ProjectMapper mapper;

    private final List<String> fields = new ArrayList<>();

    public ProjectTable(DbConnection dbConnection, ProjectMapper mapper) {

        super("project", dbConnection, new AlphabeticalOrderAliasGenerator());
        this.mapper = mapper;

        fields.add("name");
        fields.add("color");
    }

    @Override
    public Optional<Project> find(long id) throws SQLException {

        return super.find(id, mapper.find());
    }

    @Override
    public List<Project> findAll() throws SQLException {

        return super.findAll(mapper.find());
    }

    @Override
    public Optional<Project> create(Project project) throws SQLException {

        return super.create(
                fields,
                mapper.update(project)
        );
    }

    @Override
    public int update(Project project) throws SQLException {

        return super.update(
                project.id(),
                fields,
                mapper.update(project)
        );
    }

    public Optional<Project> findByProjectName(String name) throws SQLException {

        try (
                Connection connection = getNewConnection();
                PreparedStatement stmt = connection.prepareStatement("select * from %s where name = ?".formatted(getTableName()))
        ) {
            stmt.setString(1, name);

            try (ResultSet resultSet = stmt.executeQuery()) {

                if (resultSet.next()) {
                    Project project = new Project(
                            resultSet.getLong("id"),
                            resultSet.getString("name"),
                            resultSet.getString("color")
                    );

                    return Optional.of(project);
                }
            }

            return Optional.empty();
        }
    }
}



