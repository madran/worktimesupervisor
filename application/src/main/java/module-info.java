module ninja.madran.worktimesupervisor.module.application {

    exports ninja.madran.worktimesupervisor.application.api;
    exports ninja.madran.worktimesupervisor.application.config;
    exports ninja.madran.worktimesupervisor.application.lib.db;
    exports ninja.madran.worktimesupervisor.application.lib.db.alias;
    exports ninja.madran.worktimesupervisor.application.lib.detector;
    exports ninja.madran.worktimesupervisor.application.lib.interval;
    exports ninja.madran.worktimesupervisor.application.lib.mapper;
    exports ninja.madran.worktimesupervisor.application.lib.service;
    exports ninja.madran.worktimesupervisor.application.model.category;
    exports ninja.madran.worktimesupervisor.application.model.phase;
    exports ninja.madran.worktimesupervisor.application.model.task;
    exports ninja.madran.worktimesupervisor.application.model.project;

    opens ninja.madran.worktimesupervisor.application.lib.db.alias;
    opens ninja.madran.worktimesupervisor.application.lib.interval;
    opens ninja.madran.worktimesupervisor.application.lib.mapper;
    opens ninja.madran.worktimesupervisor.application.lib.service;
    opens ninja.madran.worktimesupervisor.application.model.category;
    opens ninja.madran.worktimesupervisor.application.model.phase;
    opens ninja.madran.worktimesupervisor.application.model.task;
    opens ninja.madran.worktimesupervisor.application.model.project;

    requires java.sql;
    requires ninja.madran.worktimesupervisor.module.domain;
}