package ninja.madran.worktimesupervisor.ui.component.main.category;

import javafx.scene.control.Skin;
import javafx.scene.control.TabPane;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;

public class CategoryComponent extends TabPane {

    @Override
    protected Skin<?> createDefaultSkin() {

        return new CategorySkin(this, ServiceContainer.INSTANCE);
    }
}
