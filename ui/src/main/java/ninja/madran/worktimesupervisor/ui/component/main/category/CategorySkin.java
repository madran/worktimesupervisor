package ninja.madran.worktimesupervisor.ui.component.main.category;

import javafx.collections.ListChangeListener;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Tab;
import javafx.scene.control.skin.TabPaneSkin;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.model.category.CategoryTable;
import ninja.madran.worktimesupervisor.domain.model.Project;
import ninja.madran.worktimesupervisor.ui.factory.AlertFactory;
import ninja.madran.worktimesupervisor.ui.model.CategoryList;

import java.io.IOException;
import java.lang.reflect.Field;

public class CategorySkin extends TabPaneSkin {

    public CategorySkin(CategoryComponent categoryComponent, ServiceContainer services) {

        super(categoryComponent);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/main/category.fxml"));
        loader.setControllerFactory(param -> new CategoryController(
                (CategoryTable) services.get(CategoryTable.class.getName()),
                (AlertFactory) services.get(AlertFactory.class.getName()),
                (CategoryList) services.get(CategoryList.class.getName()),
                (Project) services.get("project")
        ));
        loader.setRoot(categoryComponent);

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Pane tabContainer = (Pane) getChildren().get(getChildren().size() - 1);
        Pane tabs = (Pane) tabContainer.getChildren().get(1);

        for (Node tab : tabs.getChildren()) {
            tab.addEventHandler(MouseEvent.MOUSE_CLICKED, this::middleMouseClickedRemoveTab);
        }

        tabs.getChildren().addListener(this::addCloseTabEvent);
    }

    private void middleMouseClickedRemoveTab(MouseEvent event) {

        if (event.getButton().equals(MouseButton.MIDDLE)) {
            Parent tabControl = ((Node) event.getTarget()).getParent().getParent().getParent();

            Field[] fields = tabControl.getClass().getDeclaredFields();

            for (Field field : fields) {
                if (field.getType().equals(Tab.class)) {
                    try {
                        field.setAccessible(true);
                        CategoryTab tab = (CategoryTab) field.get(tabControl);
                        getSkinnable().getTabs().remove(tab);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void addCloseTabEvent(ListChangeListener.Change<? extends Node> change) {

        while (change.next()) {
            if (change.wasAdded()) {
                for (Node tab : change.getAddedSubList()) {
                    tab.addEventHandler(MouseEvent.MOUSE_CLICKED, this::middleMouseClickedRemoveTab);
                }
            }
        }
    }
}
