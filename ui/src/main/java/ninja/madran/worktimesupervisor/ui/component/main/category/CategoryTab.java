package ninja.madran.worktimesupervisor.ui.component.main.category;

import javafx.scene.control.Tab;
import ninja.madran.worktimesupervisor.domain.model.Category;

public class CategoryTab extends Tab {

    private Category category;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
