package ninja.madran.worktimesupervisor.ui.component.login;

import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.model.project.ProjectTable;
import ninja.madran.worktimesupervisor.domain.model.Project;
import ninja.madran.worktimesupervisor.ui.component.common.popup.info.InfoComponent;
import ninja.madran.worktimesupervisor.ui.component.main.MainWindowComponent;
import ninja.madran.worktimesupervisor.ui.factory.AlertFactory;

import java.net.URL;
import java.sql.SQLException;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.ResourceBundle;

public class LoginWindowController implements Initializable {

    @FXML
    private TextField usernameField;

    @FXML
    private Text errorMsg;

    @FXML
    public HBox inputContainer;

    private final ProjectTable projectTable;

    private final AlertFactory alertFactory;

    private final ServiceContainer services;

    public LoginWindowController(ProjectTable projectTable, AlertFactory alertFactory, ServiceContainer services) {

        this.projectTable = projectTable;
        this.alertFactory = alertFactory;
        this.services = services;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        usernameField.focusedProperty().addListener(this::onUsernameFieldFocusHideErrorMessage);

        InfoComponent infoComponent = new InfoComponent();
        infoComponent.setText("""
                Max length: 30
                Legal characters: only letters and underscore"""
        );
        inputContainer.getChildren().add(infoComponent);
    }

    @FXML
    private void login() {

        String projectName = usernameField.getText();

        if (projectName.length() == 0) {
            errorMsg.setText("You must provide a project name.");
            return;
        }

        if (projectName.length() > 30) {
            errorMsg.setText("The project name is too long.");
            return;
        }

        if (!projectName.matches("^[a-zA-Z_]+$")) {
            errorMsg.setText("You can only use letters and underscore.");
            return;
        }

        try {
            Optional<Project> project = projectTable.findByProjectName(projectName);

            services.add("project", project.orElseThrow());

            Scene scene = inputContainer.getScene();
            scene.setRoot(new MainWindowComponent());
            scene.getWindow().sizeToScene();
        } catch (SQLException e) {
            alertFactory.error("Database error", "Can't connect to database").show();
        } catch (NoSuchElementException e) {
            Optional<ButtonType> confirmation = alertFactory.confirm(
                    "Project %s doesn't exist.".formatted(projectName),
                    "Do you want to create new user with name %s?".formatted(projectName)
            ).showAndWait();

            confirmation.ifPresent(buttonType -> {
                if (buttonType.equals(ButtonType.OK)) {
                    Project project = new Project(projectName);
                    try {
                        projectTable.create(project);
                    } catch (SQLException sqlException) {
                        alertFactory.databaseErrorCreate().show();
                    }

                    alertFactory.info(null, "User created").show();
                }
            });
        }
    }

    @FXML
    public void loginOnEnterKeyPress(KeyEvent event) {

        if (event.getCode().equals(KeyCode.ENTER)) {
            login();
        }
    }

    private void onUsernameFieldFocusHideErrorMessage(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

        if (newValue) errorMsg.setText("");
    }


}
