package ninja.madran.worktimesupervisor.ui.component.common.popup;

import javafx.scene.control.Control;
import javafx.scene.control.Skin;

import java.util.Objects;

public class PopupContentComponent extends Control {

    private String text = "";

    @Override
    public String getUserAgentStylesheet() {

        return Objects.requireNonNull(getClass().getResource("/css/common/popup-content.css")).toExternalForm();
    }

    @Override
    protected Skin<?> createDefaultSkin() {

        return new PopupContentSkin(this);
    }

    public String getText() {

        return text;
    }

    public void setText(String text) {

        this.text = text;
    }
}
