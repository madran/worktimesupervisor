package ninja.madran.worktimesupervisor.ui.component.common.window;

import javafx.scene.control.Control;
import javafx.scene.control.SkinBase;

class ButtonSkin extends SkinBase<Control> {

    ButtonSkin(Control windowBarButtonClose) {

        super(windowBarButtonClose);

        windowBarButtonClose.getStyleClass().add("bar-button");
        windowBarButtonClose.getStyleClass().add("bar-button-close");
    }
}
