package ninja.madran.worktimesupervisor.ui.component.main.task;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.domain.model.Task;

import java.util.Objects;

public class TaskComponent extends Control {

    public static final String ICON_START = "\uf04b";

    public static final String ICON_PAUSE = "\uf04c";

    public static final String ICON_HIDE = "\uf070";

    public static final String ICON_DELETE = "\uf1f8";

    private final StringProperty name = new SimpleStringProperty();

    private final BooleanProperty isActive = new SimpleBooleanProperty();

    private final Task task;

    private final ObservableList<Task> tasks;

    public TaskComponent(Task task, ObservableList<Task> tasks) {

        name.setValue(task.name());
        this.task = task;
        this.tasks = tasks;
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public Task getTask() {
        return task;
    }

    public void pause() {

        isActive.setValue(false);
    }

    public void start() {

        isActive.setValue(true);
    }

    public BooleanProperty isActiveProperty() {

        return isActive;
    }

    public void hide() {

        tasks.remove(task);
    }

    @Override
    protected Skin<?> createDefaultSkin() {

        return new TaskSkin(this, ServiceContainer.INSTANCE);
    }

    @Override
    public String getUserAgentStylesheet() {

        return Objects.requireNonNull(getClass().getResource("/css/main/task.css")).toExternalForm();
    }
}
