package ninja.madran.worktimesupervisor.ui.component.summary.tasktime;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;

import java.util.Objects;

public class TaskTimeComponent extends Control {

    private final StringProperty taskName = new SimpleStringProperty();

    private final LongProperty minutes = new SimpleLongProperty();

    public TaskTimeComponent(String taskName, long minutes) {

        this.taskName.setValue(taskName);
        this.minutes.setValue(minutes);
    }

    public StringProperty taskNameProperty() {

        return taskName;
    }

    public LongProperty minutesProperty() {

        return minutes;
    }

    @Override
    protected Skin<?> createDefaultSkin() {

        return new TaskTimeSkin(this);
    }

    @Override
    public String getUserAgentStylesheet() {

        return Objects.requireNonNull(getClass().getResource("/css/summary/task-time.css")).toExternalForm();
    }
}
