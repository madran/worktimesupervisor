package ninja.madran.worktimesupervisor.ui.component.main.taskaddinput;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import ninja.madran.worktimesupervisor.application.api.Client;
import ninja.madran.worktimesupervisor.application.model.task.TaskTable;
import ninja.madran.worktimesupervisor.domain.model.Category;
import ninja.madran.worktimesupervisor.domain.model.Task;
import ninja.madran.worktimesupervisor.ui.factory.AlertFactory;

import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Optional;
import java.util.ResourceBundle;

public class TaskAddInputController implements Initializable {

    @FXML
    private ComboBox<String> taskComboBox;

    private final ObservableList<Task> tasks;

    private final Category category;

    private final AlertFactory alertFactory;

    private final TaskTable taskTable;

    private final Client client;

    public TaskAddInputController(ObservableList<Task> tasks, Category category, AlertFactory alertFactory, TaskTable taskTable, Client client) {

        this.tasks = tasks;
        this.category = category;
        this.alertFactory = alertFactory;
        this.taskTable = taskTable;
        this.client = client;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        taskComboBox.getEditor().setOnKeyReleased(this::filterTask);
    }

    private void filterTask(KeyEvent keyEvent) {

        int keyCode = keyEvent.getCode().getCode();

        if ((keyCode >= 44 && keyCode <= 111) || keyCode == 192 || keyCode == 222) {
            taskComboBox.setItems(FXCollections.observableArrayList(client.loadTasks(taskComboBox.getEditor().getText())));
            taskComboBox.show();
        }

        if (keyEvent.getCode() == KeyCode.ENTER) {
            addTask();
        }
    }

    private void addTask() {

        String taskName = taskComboBox.getValue();

        if (taskName.isBlank()) {
            alertFactory.error("Invalid name", "Task name can't be blank.").showAndWait();
            return;
        }

        Task task = new Task(taskName, Timestamp.from(Instant.now()), category);

        try {
            Optional<Task> savedTask = taskTable.create(task);
            savedTask.ifPresent(tasks::add);
        } catch (SQLException sqlException) {
            alertFactory.databaseErrorCreate().show();
        }
    }
}
