package ninja.madran.worktimesupervisor.ui.config;

import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.ui.component.main.task.ActiveTask;
import ninja.madran.worktimesupervisor.ui.factory.AlertBuilder;
import ninja.madran.worktimesupervisor.ui.factory.AlertFactory;
import ninja.madran.worktimesupervisor.ui.factory.DialogFactory;
import ninja.madran.worktimesupervisor.ui.model.CategoryList;

public class FrontendServices {

    public static void load(ServiceContainer services) {

        services.add(
                AlertFactory.class.getName(),
                new AlertFactory(new AlertBuilder())
        );

        services.add(
                DialogFactory.class.getName(),
                new DialogFactory()
        );

        services.add(
                CategoryList.class.getName(),
                new CategoryList()
        );

        services.add(
                ActiveTask.class.getName(),
                new ActiveTask()
        );
    }
}
