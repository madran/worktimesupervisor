package ninja.madran.worktimesupervisor.ui.component.common.popup.info;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import ninja.madran.worktimesupervisor.ui.component.common.popup.PopupContentComponent;

import java.net.URL;
import java.util.ResourceBundle;

public class InfoController implements Initializable {

    @FXML
    private StackPane container;

    @FXML
    private Text symbol;

    private final Popup popup = new Popup();

    public InfoController(String text) {

        PopupContentComponent popup = new PopupContentComponent();
        popup.setText(text);

        this.popup.getContent().add(popup);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        symbol.setText("\uf05a");
    }

    public void showText(MouseEvent mouseEvent) {

        Bounds bounds = container.getBoundsInLocal();
        Bounds screenBounds = container.localToScreen(bounds);
        popup.show(symbol, screenBounds.getMinX(), screenBounds.getMinY() + screenBounds.getHeight());
    }

    public void hideText(MouseEvent mouseEvent) {
        popup.hide();
    }
}
