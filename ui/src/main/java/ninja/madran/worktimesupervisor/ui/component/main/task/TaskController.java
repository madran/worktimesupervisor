package ninja.madran.worktimesupervisor.ui.component.main.task;

import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import ninja.madran.worktimesupervisor.application.model.phase.PhaseTable;
import ninja.madran.worktimesupervisor.application.model.task.TaskTable;
import ninja.madran.worktimesupervisor.domain.model.Phase;
import ninja.madran.worktimesupervisor.ui.factory.AlertFactory;

import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

public class TaskController implements Initializable {

    @FXML
    private HBox taskNameContainer;

    @FXML
    private Button startButton;

    @FXML
    private Button hideButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Label taskNameLabel;

    private final TaskComponent taskComponent;

    private final ActiveTask activeTask;

    private final TaskTable taskTable;

    private final AlertFactory alertFactory;

    private final PhaseTable phaseTable;

    private Phase currentPhase;

    public TaskController(TaskComponent taskComponent, ActiveTask activeTask, TaskTable taskTable, AlertFactory alertFactory, PhaseTable phaseTable) {

        this.taskComponent = taskComponent;
        this.activeTask = activeTask;
        this.taskTable = taskTable;
        this.alertFactory = alertFactory;
        this.phaseTable = phaseTable;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        startButton.setText(TaskComponent.ICON_START);
        hideButton.setText(TaskComponent.ICON_HIDE);
        deleteButton.setText(TaskComponent.ICON_DELETE);

        taskNameLabel.textProperty().bind(taskComponent.nameProperty());
        taskNameLabel.heightProperty().addListener(this::resizeTaskNameContainer);

        taskComponent.isActiveProperty().addListener(this::changeActivationState);
    }

    public void start(MouseEvent event) {

        if (taskComponent.isActiveProperty().get()) {
            taskComponent.pause();
        } else {
            taskComponent.start();
        }
    }

    public void hide(MouseEvent event) {

        taskComponent.hide();
    }

    public void delete(MouseEvent event) {

        Optional<ButtonType> confirmation = alertFactory.confirm("Task delete", "Do you want to delete task?").showAndWait();

        confirmation.ifPresent(buttonType -> {
            if (buttonType.equals(ButtonType.OK)) {
                try {
                    taskTable.delete(taskComponent.getTask().id());
                    hide(event);
                } catch (SQLException sqlException) {
                    alertFactory.databaseErrorDelete().showAndWait();
                }
            }
        });
    }

    private void changeActivationState(ObservableValue<? extends Boolean> c, Boolean oldValue, Boolean newValue) {

        activeTask.getComponent().ifPresent(this::pauseActiveTask);

        if (newValue) {
            try {
                Optional<Phase> newPhase = phaseTable.startPhase(taskComponent.getTask());
                newPhase.ifPresentOrElse(this::startPhase, this::showStartPhaseError);
            } catch (SQLException sqlException) {
                alertFactory.databaseErrorCreate().showAndWait();
            }
        } else {
            try {
                phaseTable.endPhase(currentPhase);
                startButton.setText(TaskComponent.ICON_START);
            } catch (SQLException sqlException) {
                alertFactory.databaseErrorCreate().showAndWait();
            }
        }
    }

    private void pauseActiveTask(TaskComponent activeTaskComponent) {

        if (activeTaskComponent != taskComponent) {
            activeTaskComponent.pause();
        }
    }

    private void startPhase(Phase phase) {

        currentPhase = phase;
        startButton.setText(TaskComponent.ICON_PAUSE);
        activeTask.setComponent(taskComponent);
    }

    private void showStartPhaseError() {

        alertFactory.error("Phase start error", "Phase didn't start").showAndWait();
    }

    private void resizeTaskNameContainer(ObservableValue<? extends Number> observableValue, Number oldVal, Number newVal) {

        taskNameContainer.setPrefHeight(newVal.doubleValue() + taskNameLabel.getFont().getSize());
    }
}
