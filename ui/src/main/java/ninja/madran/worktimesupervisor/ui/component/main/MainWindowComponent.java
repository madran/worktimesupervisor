package ninja.madran.worktimesupervisor.ui.component.main;

import javafx.scene.control.Control;
import javafx.scene.control.Skin;

import java.util.Objects;

public class MainWindowComponent extends Control {

    @Override
    public String getUserAgentStylesheet() {

        return Objects.requireNonNull(getClass().getResource("/css/main/main.css")).toExternalForm();
    }

    @Override
    protected Skin<?> createDefaultSkin() {

        return new MainWindowSkin(this);
    }
}
