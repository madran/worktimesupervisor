package ninja.madran.worktimesupervisor.ui.component.main.task;

import java.util.Optional;

public class ActiveTask {

    private TaskComponent component;

    public Optional<TaskComponent> getComponent() {

        return Optional.ofNullable(component);
    }

    public void setComponent(TaskComponent component) {

        this.component = component;
    }
}
