package ninja.madran.worktimesupervisor.ui.component.main.task;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.AnchorPane;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.model.phase.PhaseTable;
import ninja.madran.worktimesupervisor.application.model.task.TaskTable;
import ninja.madran.worktimesupervisor.ui.factory.AlertBuilder;
import ninja.madran.worktimesupervisor.ui.factory.AlertFactory;

import java.io.IOException;

public class TaskSkin extends SkinBase<TaskComponent> {

    public TaskSkin(TaskComponent taskComponent, ServiceContainer services) {

        super(taskComponent);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/main/task.fxml"));
        loader.setControllerFactory(param -> new TaskController(
                taskComponent,
                (ActiveTask) services.get(ActiveTask.class.getName()),
                (TaskTable) services.get(TaskTable.class.getName()),
                new AlertFactory(new AlertBuilder()),
                (PhaseTable) services.get(PhaseTable.class.getName())
        ));

        try {
            AnchorPane content = loader.load();
            getChildren().add(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
