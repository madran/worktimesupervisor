package ninja.madran.worktimesupervisor.ui.component.common.window;

import javafx.scene.control.SkinBase;
import javafx.scene.layout.AnchorPane;

class BarSkin extends SkinBase<Bar> {

    BarSkin(Bar bar) {

        super(bar);

        bar.getStyleClass().add("window-bar");

        AnchorPane container = new AnchorPane();
        container.prefWidthProperty().bindBidirectional(bar.prefHeightProperty());
        container.prefHeightProperty().bindBidirectional(bar.prefHeightProperty());

        Button close = new Button();
        Button minimize = new Button();
        Button maximize = new Button();
        Button options = new Button();

        container.getChildren().addAll(options, minimize, maximize, close);

        AnchorPane.setTopAnchor(options, 0.0);
        AnchorPane.setTopAnchor(minimize, 0.0);
        AnchorPane.setTopAnchor(maximize, 0.0);
        AnchorPane.setTopAnchor(close, 0.0);

        AnchorPane.setLeftAnchor(options, 0.0);

        AnchorPane.setRightAnchor(close, 0.0);
        AnchorPane.setRightAnchor(maximize, 50.0);
        AnchorPane.setRightAnchor(minimize, 100.0);

        getChildren().add(container);
    }
}
