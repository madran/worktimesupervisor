package ninja.madran.worktimesupervisor.ui.component.summary.tasktime;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.SkinBase;

import java.io.IOException;

public class TaskTimeSkin extends SkinBase<TaskTimeComponent> {

    protected TaskTimeSkin(TaskTimeComponent taskTimeComponent) {

        super(taskTimeComponent);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/summary/task-time.fxml"));
        loader.setControllerFactory(param -> new TaskTimeController(
                taskTimeComponent
        ));

        try {
            getChildren().add(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
