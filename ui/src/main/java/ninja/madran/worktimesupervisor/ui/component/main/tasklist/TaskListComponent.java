package ninja.madran.worktimesupervisor.ui.component.main.tasklist;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.domain.model.Category;
import ninja.madran.worktimesupervisor.domain.model.Task;

import java.util.Objects;

public class TaskListComponent extends Control {

    private final Category category;

    private final ObservableList<Task> tasks = FXCollections.observableArrayList();

    public TaskListComponent(Category category) {

        this.category = category;
    }

    public Category getCategory() {

        return category;
    }

    public ObservableList<Task> getTasks() {

        return tasks;
    }

    @Override
    protected Skin<?> createDefaultSkin() {

        return new TaskListSkin(this, ServiceContainer.INSTANCE);
    }

    @Override
    public String getUserAgentStylesheet() {

        return Objects.requireNonNull(getClass().getResource("/css/main/task-list.css")).toExternalForm();
    }
}
