package ninja.madran.worktimesupervisor.ui.component.summary.tasktimelist;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.SkinBase;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.model.phase.PhaseTable;
import ninja.madran.worktimesupervisor.ui.factory.AlertFactory;

import java.io.IOException;

public class TaskTimeListSkin extends SkinBase<TaskTimeListComponent> {

    protected TaskTimeListSkin(TaskTimeListComponent control, ServiceContainer services) {

        super(control);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/summary/task-time-list.fxml"));
        loader.setControllerFactory(param -> new TaskTimeListController(
                (PhaseTable) services.get(PhaseTable.class.getName()),
                (AlertFactory) services.get(AlertFactory.class.getName())
        ));

        try {
            getChildren().add(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
