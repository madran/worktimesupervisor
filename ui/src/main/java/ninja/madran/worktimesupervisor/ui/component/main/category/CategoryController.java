package ninja.madran.worktimesupervisor.ui.component.main.category;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import ninja.madran.worktimesupervisor.application.model.category.CategoryTable;
import ninja.madran.worktimesupervisor.domain.model.Category;
import ninja.madran.worktimesupervisor.domain.model.Project;
import ninja.madran.worktimesupervisor.ui.component.main.tasklist.TaskListComponent;
import ninja.madran.worktimesupervisor.ui.factory.AlertFactory;
import ninja.madran.worktimesupervisor.ui.model.CategoryList;

import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

public class CategoryController implements Initializable {

    @FXML
    private CategoryComponent categoryComponent;

    private final CategoryTable categoryTable;

    private final AlertFactory alertFactory;

    private final ObservableList<Category> categories;

    private final Project project;

    public CategoryController(CategoryTable categoryTable, AlertFactory alertFactory, CategoryList categories, Project project) {

        this.categoryTable = categoryTable;
        this.alertFactory = alertFactory;
        this.categories = categories;
        this.project = project;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        categories.addListener(this::addCategoryTab);

        try {
            categories.addAll(categoryTable.findAllByProjectId(project.id()));
        } catch (
                SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    private void addCategoryTab(ListChangeListener.Change<? extends Category> change) {
        while (change.next()) {
            if (change.wasAdded()) {
                for (Category category : change.getAddedSubList()) {
                    CategoryTab tab = new CategoryTab();
                    tab.setText(category.name());
                    tab.setCategory(category);
                    tab.setClosable(false);

                    MenuItem menuItemHide = new MenuItem("Hide");
                    menuItemHide.setOnAction(this::hideCategory);
                    menuItemHide.setUserData(tab);

                    MenuItem menuItemDelete = new MenuItem("Delete");
                    menuItemDelete.setOnAction(this::menuDeleteCategory);
                    menuItemDelete.setUserData(tab);

                    ContextMenu contextMenu = new ContextMenu();
                    contextMenu.getItems().addAll(menuItemHide, menuItemDelete);

                    tab.setContextMenu(contextMenu);
                    tab.setContent(new TaskListComponent(category));

                    categoryComponent.getTabs().add(tab);
                }
            }
        }
    }

    private void hideCategory(Event actionEvent) {

        MenuItem menuItem = (MenuItem) actionEvent.getTarget();
        Tab tab = (Tab) menuItem.getUserData();
        categoryComponent.getTabs().remove(tab);
    }

    private void menuDeleteCategory(ActionEvent actionEvent) {

        MenuItem menuItem = (MenuItem) actionEvent.getTarget();
        CategoryTab tab = (CategoryTab) menuItem.getUserData();

        Optional<ButtonType> confirmation = alertFactory.confirm("Category delete", "Do you want to delete category \"%s\"?".formatted(tab.getCategory().name())).showAndWait();

        confirmation.ifPresent(buttonType -> {
            if (buttonType == ButtonType.OK) {
                categoryComponent.getTabs().remove(tab);
                try {
                    categoryTable.delete(tab.getCategory().id());
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
            }
        });
    }
}
