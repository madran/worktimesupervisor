package ninja.madran.worktimesupervisor.ui.component.main.menu;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ninja.madran.worktimesupervisor.application.model.category.CategoryTable;
import ninja.madran.worktimesupervisor.application.model.phase.PhaseTable;
import ninja.madran.worktimesupervisor.domain.model.Category;
import ninja.madran.worktimesupervisor.domain.model.Project;
import ninja.madran.worktimesupervisor.ui.component.summary.SummaryComponent;
import ninja.madran.worktimesupervisor.ui.factory.AlertFactory;
import ninja.madran.worktimesupervisor.ui.factory.DialogFactory;
import org.scenicview.ScenicView;

import java.net.URL;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;

public class MainMenuController implements Initializable {

    @FXML
    private Button addCategoryButton;

    private final CategoryTable categoryTable;

    private final DialogFactory dialogFactory;

    private final ObservableList<Category> categories;

    private final AlertFactory alertFactory;

    private final PhaseTable phaseTable;

    private final Project project;

    public MainMenuController(CategoryTable categoryTable, DialogFactory dialogFactory, ObservableList<Category> categories, AlertFactory alertFactory, PhaseTable phaseTable, Project project) {

        this.categoryTable = categoryTable;
        this.dialogFactory = dialogFactory;
        this.categories = categories;
        this.alertFactory = alertFactory;
        this.phaseTable = phaseTable;
        this.project = project;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void addCategory(MouseEvent event) {

        Optional<String> categoryName = dialogFactory.inputDialog("Create new category", "Write category name.").showAndWait();

        categoryName.ifPresent(this::addCategoryToDatabase);
    }

    private void addCategoryToDatabase(String categoryName) {

        try {
            Optional<Category> result = categoryTable.findByName(categoryName);

            if (result.isEmpty()) {
                Category category = new Category(categoryName, project);

                Optional<Category> categorySaved = categoryTable.create(category);

                categorySaved.ifPresent(categories::add);
            } else {
                alertFactory.error("Name exist", "Name %s already exist, create different name.").showAndWait();
                addCategoryButton.fireEvent(
                        new MouseEvent(MouseEvent.MOUSE_CLICKED, 0, 0, 0, 0, MouseButton.PRIMARY, 1, true, true, true, true, true, true, true, true, true, true, null)
                );
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

    }

    public void showProgressSummary(MouseEvent mouseEvent) {

        try {
            boolean hasUnsavedPhases = phaseTable.hasUnsavedPhases();

            if (!hasUnsavedPhases) {
                alertFactory.info("No progress", "You haven't made any new progress in tasks to save.");
                return;
            }

            SummaryComponent summaryComponent = new SummaryComponent();

            Scene scene = new Scene(summaryComponent);
            scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("/css/base.css")).toExternalForm());

            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(((Node) (mouseEvent.getSource())).getScene().getWindow());
            stage.show();

        } catch (SQLException sqlException) {
            alertFactory.databaseErrorLoad().showAndWait();
        }
    }

    public void showScenicView(MouseEvent mouseEvent) {

        ScenicView.show(addCategoryButton.getScene());
    }
}
