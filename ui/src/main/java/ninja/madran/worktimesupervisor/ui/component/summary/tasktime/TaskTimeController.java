package ninja.madran.worktimesupervisor.ui.component.summary.tasktime;

import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

public class TaskTimeController implements Initializable {

    @FXML
    private VBox sliderContainer;

    @FXML
    private Label taskNameLabel;

    private final TaskTimeComponent taskTimeComponent;

    public TaskTimeController(TaskTimeComponent taskTimeComponent) {

        this.taskTimeComponent = taskTimeComponent;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        taskNameLabel.textProperty().bind(taskTimeComponent.taskNameProperty());
        taskNameLabel.heightProperty().addListener(this::resizeSliderContainer);
    }

    private void resizeSliderContainer(ObservableValue<? extends Number> observableValue, Number oldVal, Number newVal) {

        sliderContainer.setPrefHeight(newVal.doubleValue() + taskNameLabel.getFont().getSize());
    }
}
