package ninja.madran.worktimesupervisor.ui.component.main;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class MainWindowSkin extends SkinBase<MainWindowComponent> {

    public MainWindowSkin(MainWindowComponent mainWindowComponent) {

        super(mainWindowComponent);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/main/main.fxml"));
        loader.setControllerFactory(param -> new MainWindowController());

        try {
            AnchorPane content = loader.load();
            getChildren().add(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
