package ninja.madran.worktimesupervisor.ui.component.main.tasklist;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import ninja.madran.worktimesupervisor.application.model.task.TaskTable;
import ninja.madran.worktimesupervisor.domain.model.Category;
import ninja.madran.worktimesupervisor.domain.model.Task;
import ninja.madran.worktimesupervisor.ui.component.main.task.TaskComponent;
import ninja.madran.worktimesupervisor.ui.component.main.taskaddinput.TaskAddInputComponent;

import java.net.URL;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

public class TaskListController implements Initializable {

    @FXML
    private ScrollPane taskListScroll;

    @FXML
    private VBox taskList;

    private final Category category;

    private final TaskTable taskTable;

    private final ObservableList<Task> tasks;

    public TaskListController(Category category, ObservableList<Task> tasks, TaskTable taskTable) {

        this.category = category;
        this.taskTable = taskTable;
        this.tasks = tasks;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        tasks.addListener(this::addTaskComponent);
        tasks.addListener(this::hideTaskComponent);

        VBox.setVgrow(taskList, Priority.ALWAYS);
        HBox.setHgrow(taskList, Priority.ALWAYS);

        try {
            List<Task> loadedTasks = taskTable.findAllRecentTasksByCategoryId(category.id(), 10);
            Collections.reverse(loadedTasks);

            tasks.addAll(loadedTasks);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        TaskAddInputComponent taskAddInput = new TaskAddInputComponent(tasks, category);

        VBox listContainer = (VBox) taskListScroll.getContent();
        listContainer.getChildren().add(0, taskAddInput);
    }

    private void addTaskComponent(ListChangeListener.Change<? extends Task> c) {

        while (c.next()) {
            if (c.wasAdded()) {
                for (Task task : c.getAddedSubList()) {
                    TaskComponent taskComponent = new TaskComponent(task, tasks);
                    taskList.getChildren().add(0, taskComponent);
                }
            }
        }
    }

    private void hideTaskComponent(ListChangeListener.Change<? extends Task> c) {

        while (c.next()) {
            if (c.wasRemoved()) {
                for (Task task : c.getRemoved()) {
                    taskList.getChildren().removeIf(
                            (Node taskComponent) -> {
                                if (taskComponent instanceof TaskComponent) {
                                    return ((TaskComponent) taskComponent).getTask().equals(task);
                                }

                                return false;
                            }
                    );
                }
            }
        }
    }
}
