package ninja.madran.worktimesupervisor.ui.factory;

import javafx.scene.control.Alert;

public class AlertFactory {

    private final AlertBuilder alertBuilder;

    public AlertFactory(AlertBuilder alertBuilder) {

        this.alertBuilder = alertBuilder;
    }

    public Alert info(String header, String content) {

        return alertBuilder
                .setTitle("Information message")
                .setHeaderText(header)
                .setContent(content)
                .build(Alert.AlertType.INFORMATION);
    }

    public Alert error(String header, String content) {

        return alertBuilder
                .setTitle("Error message")
                .setHeaderText(header)
                .setContent(content)
                .build(Alert.AlertType.ERROR);
    }

    public Alert confirm(String header, String content) {

        return alertBuilder
                .setTitle("Confirmation message")
                .setHeaderText(header)
                .setContent(content)
                .build(Alert.AlertType.CONFIRMATION);
    }

    public Alert databaseErrorCreate() {

        return error("Database error", "Can't save data");
    }

    public Alert databaseErrorDelete() {

        return error("Database error", "Can't delete data");
    }

    public Alert databaseErrorLoad() {

        return error("Database error", "Can't load data");
    }
}
