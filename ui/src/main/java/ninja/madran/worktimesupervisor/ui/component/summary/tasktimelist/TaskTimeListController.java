package ninja.madran.worktimesupervisor.ui.component.summary.tasktimelist;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import ninja.madran.worktimesupervisor.application.lib.interval.TimeInterval;
import ninja.madran.worktimesupervisor.application.lib.mapper.MapperInsufficientDataException;
import ninja.madran.worktimesupervisor.application.model.phase.PhaseTable;
import ninja.madran.worktimesupervisor.domain.model.Task;
import ninja.madran.worktimesupervisor.ui.component.summary.tasktime.TaskTimeComponent;
import ninja.madran.worktimesupervisor.ui.factory.AlertFactory;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

public class TaskTimeListController implements Initializable {

    @FXML
    private VBox taskTimeListContainer;

    private final PhaseTable phaseTable;

    private final AlertFactory alertFactory;

    public TaskTimeListController(PhaseTable phaseTable, AlertFactory alertFactory) {

        this.phaseTable = phaseTable;
        this.alertFactory = alertFactory;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            List<TimeInterval<Task>> tasksWorkTime = phaseTable.getUnsavedTasksWorkTime();

            for (TimeInterval<Task> timeInterval : tasksWorkTime) {
                TaskTimeComponent taskTimeComponent = new TaskTimeComponent(
                        timeInterval.getId().name(),
                        timeInterval.getDuration().toMinutes()
                );
                taskTimeListContainer.getChildren().add(taskTimeComponent);
            }
        } catch (SQLException sqlException) {
            alertFactory.info(
                    "No progress",
                    "You haven't made any new progress in tasks to save."
            ).showAndWait();
            Stage stage = (Stage) taskTimeListContainer.getScene().getWindow();
            stage.close();
        } catch (MapperInsufficientDataException e) {
            e.printStackTrace();
        }
    }
}
