package ninja.madran.worktimesupervisor.ui.component.login;

import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;

import java.util.Objects;

public class LoginWindowComponent extends Control {

    @Override
    protected Skin<?> createDefaultSkin() {

        return new LoginWindowSkin(this, ServiceContainer.INSTANCE);
    }

    @Override
    public String getUserAgentStylesheet() {

        return Objects.requireNonNull(getClass().getResource("/css/login/login.css")).toExternalForm();
    }
}
