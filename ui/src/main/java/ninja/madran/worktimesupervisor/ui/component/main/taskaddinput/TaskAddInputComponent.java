package ninja.madran.worktimesupervisor.ui.component.main.taskaddinput;

import javafx.collections.ObservableList;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.domain.model.Category;
import ninja.madran.worktimesupervisor.domain.model.Task;

import java.util.Objects;

public class TaskAddInputComponent extends Control {

    private final ObservableList<Task> tasks;

    private final Category category;

    public TaskAddInputComponent(ObservableList<Task> tasks, Category category) {

        this.tasks = tasks;
        this.category = category;
    }

    public ObservableList<Task> getTasks() {
        return tasks;
    }

    public Category getCategory() {

        return category;
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new TaskAddInputSkin(this, ServiceContainer.INSTANCE);
    }

    @Override
    public String getUserAgentStylesheet() {
        return Objects.requireNonNull(getClass().getResource("/css/main/task-add-input.css")).toExternalForm();
    }
}
