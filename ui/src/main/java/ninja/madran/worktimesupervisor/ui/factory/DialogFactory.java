package ninja.madran.worktimesupervisor.ui.factory;

import javafx.scene.control.TextInputDialog;

public class DialogFactory {

    public TextInputDialog inputDialog(String header, String content) {

        TextInputDialog dialog = new TextInputDialog();
        dialog.setHeaderText(header);
        dialog.setContentText(content);

        return dialog;
    }
}
