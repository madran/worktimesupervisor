package ninja.madran.worktimesupervisor.ui.component.common.window;

public enum ButtonType {

    CLOSE, MINIMIZE, MAXIMIZE, OPTIONS
}
