package ninja.madran.worktimesupervisor.ui.component.summary.menu;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.model.phase.PhaseTable;
import ninja.madran.worktimesupervisor.ui.factory.AlertBuilder;
import ninja.madran.worktimesupervisor.ui.factory.AlertFactory;

import java.io.IOException;

public class SummaryMenuSkin extends SkinBase<SummaryMenuComponent> {

    protected SummaryMenuSkin(SummaryMenuComponent control, ServiceContainer services) {

        super(control);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/summary/summary-menu.fxml"));
        loader.setControllerFactory(param -> new SummaryMenuController(
                new AlertFactory(new AlertBuilder()),
                (PhaseTable) services.get(PhaseTable.class.getName())
        ));

        try {
            HBox content = loader.load();
            getChildren().add(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
