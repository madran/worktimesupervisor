package ninja.madran.worktimesupervisor.ui.component.login;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.StackPane;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.model.project.ProjectTable;
import ninja.madran.worktimesupervisor.ui.factory.AlertBuilder;
import ninja.madran.worktimesupervisor.ui.factory.AlertFactory;

import java.io.IOException;

public class LoginWindowSkin extends SkinBase<LoginWindowComponent> {

    protected LoginWindowSkin(LoginWindowComponent loginWindowComponent, ServiceContainer services) {

        super(loginWindowComponent);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/login/login.fxml"));
        loader.setControllerFactory(param -> new LoginWindowController(
                (ProjectTable) services.get(ProjectTable.class.getName()),
                new AlertFactory(new AlertBuilder()),
                services
        ));

        try {
            StackPane content = loader.load();
            getChildren().add(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
