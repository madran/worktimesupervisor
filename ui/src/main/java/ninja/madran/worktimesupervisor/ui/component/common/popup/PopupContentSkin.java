package ninja.madran.worktimesupervisor.ui.component.common.popup;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.io.IOException;

public class PopupContentSkin extends SkinBase<PopupContentComponent> {

    protected PopupContentSkin(PopupContentComponent popupContentComponent) {

        super(popupContentComponent);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/common/popup.fxml"));

        try {
            StackPane content = loader.load();
            Text text = (Text) content.getChildren().get(0);
            text.setText(popupContentComponent.getText());

            getChildren().add(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
