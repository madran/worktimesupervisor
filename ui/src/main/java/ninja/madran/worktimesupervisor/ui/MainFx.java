package ninja.madran.worktimesupervisor.ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import ninja.madran.worktimesupervisor.application.config.AppConfig;
import ninja.madran.worktimesupervisor.application.config.AppTestConfig;
import ninja.madran.worktimesupervisor.application.config.Services;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.ui.component.login.LoginWindowComponent;
import ninja.madran.worktimesupervisor.ui.config.FrontendServices;

import java.util.Objects;

public class MainFx extends Application {

	public static void main(String[] args) {

		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {

        AppConfig config = new AppTestConfig();
        Services.load(config, ServiceContainer.INSTANCE);
        FrontendServices.load(ServiceContainer.INSTANCE);

        Font.loadFont(Objects.requireNonNull(getClass().getResource("/font/ubuntu/Ubuntu-Regular.ttf")).toExternalForm(), 10);
        Font.loadFont(Objects.requireNonNull(getClass().getResource("/font/awesome/fa-regular-400.ttf")).toExternalForm(), 10);
        Font.loadFont(Objects.requireNonNull(getClass().getResource("/font/awesome/fa-solid-900.ttf")).toExternalForm(), 10);

        LoginWindowComponent loginWindowComponent = new LoginWindowComponent();

        Scene scene = new Scene(loginWindowComponent);
        scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("/css/base.css")).toExternalForm());

        primaryStage.setTitle("Category Time Supervisor");
        primaryStage.setScene(scene);
        primaryStage.show();

        primaryStage.setOnCloseRequest(t -> {

            Platform.exit();
            System.exit(0);
        });
    }
}