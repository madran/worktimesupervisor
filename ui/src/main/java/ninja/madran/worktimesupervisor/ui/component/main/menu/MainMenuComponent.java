package ninja.madran.worktimesupervisor.ui.component.main.menu;

import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;

import java.util.Objects;

public class MainMenuComponent extends Control {

    @Override
    protected Skin<?> createDefaultSkin() {

        return new MainMenuSkin(this, ServiceContainer.INSTANCE);
    }

    @Override
    public String getUserAgentStylesheet() {

        return Objects.requireNonNull(getClass().getResource("/css/common/menu.css")).toExternalForm();
    }
}
