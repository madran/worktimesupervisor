package ninja.madran.worktimesupervisor.ui.component.common.window;

import javafx.scene.control.Control;
import javafx.scene.control.Skin;

import java.util.Objects;

class Bar extends Control {

    @Override
    public String getUserAgentStylesheet() {

        return Objects.requireNonNull(getClass().getResource("/css/common/window-bar.css")).toExternalForm();
    }

    @Override
    protected Skin<?> createDefaultSkin() {

        return new BarSkin(this);
    }
}
