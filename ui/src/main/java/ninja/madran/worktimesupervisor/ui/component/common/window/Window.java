package ninja.madran.worktimesupervisor.ui.component.common.window;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;

import java.util.Objects;

public class Window extends Control {

    ObservableList<Node> children = FXCollections.observableArrayList();

    public Window() {

        getStyleClass().add("window");
    }

    public void addChildren(Node node) {

        children.add(node);
    }

    public ObservableList<Node> childrenProperty() {

        return children;
    }

    @Override
    public String getUserAgentStylesheet() {

        return Objects.requireNonNull(getClass().getResource("/css/common/window.css")).toExternalForm();
    }

    @Override
    protected Skin<?> createDefaultSkin() {

        return new WindowSkin(this);
    }
}
