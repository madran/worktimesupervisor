package ninja.madran.worktimesupervisor.ui.component.main.tasklist;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SkinBase;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.model.task.TaskTable;

import java.io.IOException;

public class TaskListSkin extends SkinBase<TaskListComponent> {

    public TaskListSkin(TaskListComponent taskListComponent, ServiceContainer services) {

        super(taskListComponent);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/main/task-list.fxml"));
        loader.setControllerFactory(param -> new TaskListController(
                taskListComponent.getCategory(),
                taskListComponent.getTasks(),
                (TaskTable) services.get(TaskTable.class.getName())
        ));

        try {
            ScrollPane content = loader.load();
            getChildren().add(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
