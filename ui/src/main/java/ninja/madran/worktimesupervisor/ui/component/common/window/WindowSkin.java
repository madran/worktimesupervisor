package ninja.madran.worktimesupervisor.ui.component.common.window;

import javafx.collections.ListChangeListener;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.SkinBase;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class WindowSkin extends SkinBase<Window> {

    private double startXPos;

    private double startYPos;

    private double previousXPos;

    private double previousYPos;

    private Direction direction;

    final private double WINDOW_MIN_HEIGHT = 50.0;

    final private double WINDOW_MIN_WIDTH = 200.0;

    private final double BORDER_SIZE = 5.0;

    private final double CORNER_RESIZE_SPACE = 10.0;

    private enum Direction {
        N, NE, NW, W, E, S, SE, SW
    }

    protected WindowSkin(Window window) {

        super(window);

        AnchorPane container = new AnchorPane();

        Bar windowBar = new Bar();

        container.getChildren().add(windowBar);

        AnchorPane.setTopAnchor(windowBar, 0.0);
        AnchorPane.setLeftAnchor(windowBar, 0.0);
        AnchorPane.setRightAnchor(windowBar, 0.0);

        StackPane content = new StackPane();
        content.getChildren().addAll(window.childrenProperty());
        window.childrenProperty().addListener((ListChangeListener<Node>) change -> {
            while (change.next()) {
                if (change.wasAdded()) {
                    for (Node node : change.getAddedSubList()) {
                        content.getChildren().add(node);
                    }
                }

                if (change.wasRemoved()) {
                    for (Node node : change.getRemoved()) {
                        content.getChildren().remove(node);
                    }
                }
            }
        });
        container.getChildren().add(content);
        content.setMinHeight(0.0);
        content.setMinWidth(0.0);

        AnchorPane.setTopAnchor(content, 50.0);
        AnchorPane.setLeftAnchor(content, 0.0);
        AnchorPane.setRightAnchor(content, 0.0);
        AnchorPane.setBottomAnchor(content, 0.0);

        content.backgroundProperty().set(new Background(new BackgroundFill(Color.CHOCOLATE, CornerRadii.EMPTY, Insets.EMPTY)));

        getChildren().add(container);

        window.addEventHandler(MouseEvent.MOUSE_MOVED, this::onMouseEnter);
        window.addEventHandler(MouseEvent.MOUSE_PRESSED, this::onMousePressed);
        window.addEventHandler(MouseEvent.MOUSE_DRAGGED, this::onMouseDragged);
        window.addEventHandler(MouseEvent.MOUSE_RELEASED, this::onMouseReleased);
    }

    private void onMouseEnter(MouseEvent event) {

        double x = event.getX();
        double y = event.getY();

        Scene scene = ((Window) event.getSource()).getScene();

        double sceneWidth = scene.getWidth();
        double sceneHeight = scene.getHeight();

        if (isNorthWest(x, y)) {
            scene.setCursor(Cursor.NW_RESIZE);
        } else if (isNorth(x, y, sceneWidth)) {
            scene.setCursor(Cursor.N_RESIZE);
        } else if (isNorthEast(x, y, sceneWidth)) {
            scene.setCursor(Cursor.NE_RESIZE);
        } else if (isWest(x, y, sceneHeight)) {
            scene.setCursor(Cursor.W_RESIZE);
        } else if (isEast(x, y, sceneWidth, sceneHeight)) {
            scene.setCursor(Cursor.E_RESIZE);
        } else if (isSouthWest(x, y, sceneHeight)) {
            scene.setCursor(Cursor.SW_RESIZE);
        } else if (isSouth(x, y, sceneWidth, sceneHeight)) {
            scene.setCursor(Cursor.S_RESIZE);
        } else if (isSouthEast(x, y, sceneWidth, sceneHeight)) {
            scene.setCursor(Cursor.SE_RESIZE);
        } else {
            scene.setCursor(Cursor.DEFAULT);
        }
    }

    private void onMousePressed(MouseEvent mouseEvent) {

        startXPos = mouseEvent.getScreenX();
        startYPos = mouseEvent.getScreenY();

        double x = mouseEvent.getX();
        double y = mouseEvent.getY();

        Scene scene = ((Window) mouseEvent.getSource()).getScene();

        double sceneWidth = scene.getWidth();
        double sceneHeight = scene.getHeight();

        if (isNorthWest(x, y)) {
            direction = Direction.NW;
        } else if (isNorth(x, y, sceneWidth)) {
            direction = Direction.N;
        } else if (isNorthEast(x, y, sceneWidth)) {
            direction = Direction.NE;
        } else if (isWest(x, y, sceneHeight)) {
            direction = Direction.W;
        } else if (isEast(x, y, sceneWidth, sceneHeight)) {
            direction = Direction.E;
        } else if (isSouthWest(x, y, sceneHeight)) {
            direction = Direction.SW;
        } else if (isSouth(x, y, sceneWidth, sceneHeight)) {
            direction = Direction.S;
        } else if (isSouthEast(x, y, sceneWidth, sceneHeight)) {
            direction = Direction.SE;
        }
    }

    private void onMouseReleased(MouseEvent mouseEvent) {

        previousXPos = 0.0;
        previousYPos = 0.0;
        direction = null;
    }

    private void onMouseDragged(MouseEvent event) {

        double x = event.getX();
        double y = event.getY();

        Scene scene = ((Window) event.getSource()).getScene();
        Stage stage = (Stage) scene.getWindow();

        double currentXPos = startXPos - event.getScreenX();
        double currentYPos = startYPos - event.getScreenY();

        double ay = stage.getHeight() - event.getY();

        double deltaX = previousXPos - currentXPos;
        double deltaY = previousYPos - currentYPos;

        previousXPos = currentXPos;
        previousYPos = currentYPos;

        if (direction == Direction.NW) {
            if ((event.getScreenY() - BORDER_SIZE) < stage.getY() + stage.getHeight() - WINDOW_MIN_HEIGHT) {
                resizeWindowNorth(stage, deltaY);
            }

            if ((event.getScreenX() - BORDER_SIZE) < stage.getX() + stage.getWidth() - WINDOW_MIN_WIDTH) {
                resizeWindowWest(stage, deltaX);
            }
        } else if (direction == Direction.N) {
            if ((event.getScreenY() - BORDER_SIZE) < stage.getY() + stage.getHeight() - WINDOW_MIN_HEIGHT) {
                resizeWindowNorth(stage, deltaY);
            }
        } else if (direction == Direction.NE) {
            if ((event.getScreenY() - BORDER_SIZE) < stage.getY() + stage.getHeight() - WINDOW_MIN_HEIGHT) {
                resizeWindowNorth(stage, deltaY);
            }

            if ((event.getScreenX() - BORDER_SIZE) > stage.getX() + WINDOW_MIN_WIDTH) {
                resizeWindowEast(stage, deltaX);
            }
        } else if (direction == Direction.W) {
            if ((event.getScreenX() - BORDER_SIZE) < stage.getX() + stage.getWidth() - WINDOW_MIN_WIDTH) {
                resizeWindowWest(stage, deltaX);
            }
        } else if (direction == Direction.E) {
            if ((event.getScreenX() - BORDER_SIZE) > stage.getX() + WINDOW_MIN_WIDTH) {
                resizeWindowEast(stage, deltaX);
            }
        } else if (direction == Direction.SW) {
            if ((event.getScreenY() - BORDER_SIZE) > stage.getY() + WINDOW_MIN_HEIGHT) {
                resizeWindowSouth(stage, deltaY);
            }

            if ((event.getScreenX() - BORDER_SIZE) < stage.getX() + stage.getWidth() - WINDOW_MIN_WIDTH) {
                resizeWindowWest(stage, deltaX);
            }
        } else if (direction == Direction.S) {

            if ((event.getScreenY() - BORDER_SIZE - ay) > stage.getY() + WINDOW_MIN_HEIGHT) {
                resizeWindowSouth(stage, deltaY);
            }
        } else if (direction == Direction.SE) {
            if ((event.getScreenY() - BORDER_SIZE) > stage.getY() + WINDOW_MIN_HEIGHT) {
                resizeWindowSouth(stage, deltaY);
            }

            if ((event.getScreenX() - BORDER_SIZE) > stage.getX() + WINDOW_MIN_WIDTH) {
                resizeWindowEast(stage, deltaX);
            }
        }
    }

    private void resizeWindowWest(Stage stage, double deltaX) {

        double stageWidth = stage.getWidth() - deltaX;

        if (stageWidth < WINDOW_MIN_WIDTH) {
            stageWidth = WINDOW_MIN_WIDTH;
        } else {
            stage.setX(stage.getX() + deltaX);
        }

        stage.setWidth(stageWidth);
    }

    private void resizeWindowNorth(Stage stage, double deltaY) {

        double stageHeight = stage.getHeight() - deltaY;

        if (stageHeight < WINDOW_MIN_HEIGHT) {
            stageHeight = WINDOW_MIN_HEIGHT;
        } else {
            stage.setY(stage.getY() + deltaY);
        }

        stage.setHeight(stageHeight);
    }

    private void resizeWindowEast(Stage stage, double deltaX) {

        double stageWidth = stage.getWidth() + deltaX;

        if (stageWidth < WINDOW_MIN_WIDTH) {
            stageWidth = WINDOW_MIN_WIDTH;
        }

        stage.setWidth(stageWidth);
    }

    private void resizeWindowSouth(Stage stage, double deltaY) {

        double stageHeight = stage.getHeight() + deltaY;

        if (stageHeight < WINDOW_MIN_HEIGHT) {
            stageHeight = WINDOW_MIN_HEIGHT;
        }

        stage.setHeight(stageHeight);
    }

    private boolean isSouthEast(double x, double y, double sceneWidth, double sceneHeight) {

        return x > sceneWidth - CORNER_RESIZE_SPACE && y > sceneHeight - CORNER_RESIZE_SPACE;
    }

    private boolean isSouth(double x, double y, double sceneWidth, double sceneHeight) {

        return x > CORNER_RESIZE_SPACE && x < sceneWidth - CORNER_RESIZE_SPACE && y > sceneHeight - BORDER_SIZE;
    }

    private boolean isSouthWest(double x, double y, double sceneHeight) {

        return x < CORNER_RESIZE_SPACE && y > sceneHeight - CORNER_RESIZE_SPACE;
    }

    private boolean isEast(double x, double y, double sceneWidth, double sceneHeight) {

        return x > sceneWidth - BORDER_SIZE && y > CORNER_RESIZE_SPACE && y < sceneHeight - CORNER_RESIZE_SPACE;
    }

    private boolean isWest(double x, double y, double sceneHeight) {

        return x < BORDER_SIZE && y > CORNER_RESIZE_SPACE && y < sceneHeight - CORNER_RESIZE_SPACE;
    }

    private boolean isNorthEast(double x, double y, double sceneWidth) {

        return x > sceneWidth - CORNER_RESIZE_SPACE && y < CORNER_RESIZE_SPACE;
    }

    private boolean isNorth(double x, double y, double sceneWidth) {

        return x > CORNER_RESIZE_SPACE && x < sceneWidth - CORNER_RESIZE_SPACE && y < BORDER_SIZE;
    }

    private boolean isNorthWest(double x, double y) {

        return x < CORNER_RESIZE_SPACE && y < CORNER_RESIZE_SPACE;
    }


}
