package ninja.madran.worktimesupervisor.ui.component.summary;

import javafx.scene.control.Control;
import javafx.scene.control.Skin;

import java.util.Objects;

public class SummaryComponent extends Control {

    @Override
    protected Skin<?> createDefaultSkin() {

        return new SummarySkin(this);
    }

    @Override
    public String getUserAgentStylesheet() {

        return Objects.requireNonNull(getClass().getResource("/css/summary/summary.css")).toExternalForm();
    }
}
