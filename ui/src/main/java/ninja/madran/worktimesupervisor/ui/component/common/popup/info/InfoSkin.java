package ninja.madran.worktimesupervisor.ui.component.common.popup.info;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.StackPane;

import java.io.IOException;

public class InfoSkin extends SkinBase<InfoComponent> {

    public InfoSkin(InfoComponent infoComponent) {

        super(infoComponent);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/common/info.fxml"));
        loader.setControllerFactory(param -> new InfoController(infoComponent.getText()));

        try {
            StackPane content = loader.load();
            getChildren().add(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
