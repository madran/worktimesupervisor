package ninja.madran.worktimesupervisor.ui.component.summary;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.SkinBase;

import java.io.IOException;

public class SummarySkin extends SkinBase<SummaryComponent> {

    protected SummarySkin(SummaryComponent summaryComponent) {

        super(summaryComponent);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/summary/summary.fxml"));
        loader.setControllerFactory(param -> new SummaryController());

        try {
            getChildren().add(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
