package ninja.madran.worktimesupervisor.ui.component.main.taskaddinput;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.VBox;
import ninja.madran.worktimesupervisor.application.api.ClientJira;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.model.task.TaskTable;
import ninja.madran.worktimesupervisor.ui.factory.AlertBuilder;
import ninja.madran.worktimesupervisor.ui.factory.AlertFactory;

import java.io.IOException;

public class TaskAddInputSkin extends SkinBase<TaskAddInputComponent> {

    public TaskAddInputSkin(TaskAddInputComponent taskAddInputComponent, ServiceContainer services) {

        super(taskAddInputComponent);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/main/task-add-input.fxml"));
        loader.setControllerFactory(param -> new TaskAddInputController(
                taskAddInputComponent.getTasks(),
                taskAddInputComponent.getCategory(),
                new AlertFactory(new AlertBuilder()),
                (TaskTable) services.get(TaskTable.class.getName()),
                (ClientJira) services.get(ClientJira.class.getName())
        ));

        try {
            VBox container = loader.load();
            getChildren().add(container);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
