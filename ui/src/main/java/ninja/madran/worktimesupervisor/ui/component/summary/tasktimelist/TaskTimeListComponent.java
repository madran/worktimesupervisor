package ninja.madran.worktimesupervisor.ui.component.summary.tasktimelist;

import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;

import java.util.Objects;

public class TaskTimeListComponent extends Control {

    @Override
    protected Skin<?> createDefaultSkin() {
        return new TaskTimeListSkin(this, ServiceContainer.INSTANCE);
    }

    @Override
    public String getUserAgentStylesheet() {
        return Objects.requireNonNull(getClass().getResource("/css/summary/task-time-list.css")).toExternalForm();
    }
}
