package ninja.madran.worktimesupervisor.ui.factory;

import javafx.scene.control.Alert;

public class AlertBuilder {

    private String title = "";

    private String header = "";

    private String content = "";

    public AlertBuilder setTitle(String title) {

        this.title = title;
        return this;
    }

    public AlertBuilder setHeaderText(String header) {

        this.header = header;
        return this;
    }

    public AlertBuilder setContent(String content) {

        this.content = content;
        return this;
    }

    public Alert build(Alert.AlertType alertType) {

        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);

        return alert;
    }
}
