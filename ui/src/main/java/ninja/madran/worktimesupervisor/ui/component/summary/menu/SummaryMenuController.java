package ninja.madran.worktimesupervisor.ui.component.summary.menu;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import ninja.madran.worktimesupervisor.application.model.phase.PhaseTable;
import ninja.madran.worktimesupervisor.ui.factory.AlertFactory;
import org.scenicview.ScenicView;

public class SummaryMenuController {

    private final AlertFactory alertFactory;

    private final PhaseTable phaseTable;

    @FXML
    private Button scenicViewButton;

    public SummaryMenuController(AlertFactory alertFactory, PhaseTable phaseTable) {

        this.alertFactory = alertFactory;
        this.phaseTable = phaseTable;
    }

    public void saveProgress(MouseEvent mouseEvent) {
        Node source = (Node) mouseEvent.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

    public void showScenicView(MouseEvent mouseEvent) {

        ScenicView.show(scenicViewButton.getScene());
    }
}
