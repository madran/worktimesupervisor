package ninja.madran.worktimesupervisor.ui.component.main.menu;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import ninja.madran.worktimesupervisor.application.lib.service.ServiceContainer;
import ninja.madran.worktimesupervisor.application.model.category.CategoryTable;
import ninja.madran.worktimesupervisor.application.model.phase.PhaseTable;
import ninja.madran.worktimesupervisor.domain.model.Project;
import ninja.madran.worktimesupervisor.ui.factory.AlertFactory;
import ninja.madran.worktimesupervisor.ui.factory.DialogFactory;
import ninja.madran.worktimesupervisor.ui.model.CategoryList;

import java.io.IOException;

public class MainMenuSkin extends SkinBase<MainMenuComponent> {

    public MainMenuSkin(MainMenuComponent mainMenuComponent, ServiceContainer services) {

        super(mainMenuComponent);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/main/main-menu.fxml"));
        loader.setControllerFactory(param -> new MainMenuController(
                (CategoryTable) services.get(CategoryTable.class.getName()),
                (DialogFactory) services.get(DialogFactory.class.getName()),
                (CategoryList) services.get(CategoryList.class.getName()),
                (AlertFactory) services.get(AlertFactory.class.getName()),
                (PhaseTable) services.get(PhaseTable.class.getName()),
                (Project) services.get("project")
        ));

        try {
            HBox content = loader.load();
            getChildren().add(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
