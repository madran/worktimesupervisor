package ninja.madran.worktimesupervisor.ui.model;

import javafx.collections.ModifiableObservableListBase;
import ninja.madran.worktimesupervisor.domain.model.Category;

import java.util.ArrayList;
import java.util.List;

public class CategoryList extends ModifiableObservableListBase<Category> {

    private final List<Category> list = new ArrayList<>();

    public Category get(int index) {
        return list.get(index);
    }

    public int size() {
        return list.size();
    }

    protected void doAdd(int index, Category element) {
        list.add(index, element);
    }

    protected Category doSet(int index, Category element) {
        return list.set(index, element);
    }

    protected Category doRemove(int index) {
        return list.remove(index);
    }
}
