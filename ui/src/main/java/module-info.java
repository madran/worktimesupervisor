module ninja.madran.worktimesupervisor.module.ui {
    requires java.sql;
    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.swing;
    requires javafx.web;
    requires ninja.madran.worktimesupervisor.module.application;
    requires ninja.madran.worktimesupervisor.module.domain;
    requires org.scenicview.scenicview;

    exports ninja.madran.worktimesupervisor.ui.component.main.category;
    exports ninja.madran.worktimesupervisor.ui.component.main.menu;
    exports ninja.madran.worktimesupervisor.ui.component.main.taskaddinput;
    exports ninja.madran.worktimesupervisor.ui.component.summary.menu;
    exports ninja.madran.worktimesupervisor.ui.component.summary.tasktimelist;
    exports ninja.madran.worktimesupervisor.ui.factory;
    exports ninja.madran.worktimesupervisor.ui.model;
    exports ninja.madran.worktimesupervisor.ui;

    opens ninja.madran.worktimesupervisor.ui.component.common.popup.info;
    opens ninja.madran.worktimesupervisor.ui.component.login;
    opens ninja.madran.worktimesupervisor.ui.component.main.category;
    opens ninja.madran.worktimesupervisor.ui.component.main.menu;
    opens ninja.madran.worktimesupervisor.ui.component.main.task;
    opens ninja.madran.worktimesupervisor.ui.component.main.taskaddinput;
    opens ninja.madran.worktimesupervisor.ui.component.main.tasklist;
    opens ninja.madran.worktimesupervisor.ui.component.main;
    opens ninja.madran.worktimesupervisor.ui.component.summary.menu;
    opens ninja.madran.worktimesupervisor.ui.component.summary.tasktime;
    opens ninja.madran.worktimesupervisor.ui.component.summary.tasktimelist;
}